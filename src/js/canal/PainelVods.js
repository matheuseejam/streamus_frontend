import React, { Component } from 'react';
import LoadingCardVod from '../loadingComponents/LoadingCardVod.js';
import FiltroVODPorJogos from './FiltroVODPorJogos.js';
import Estatisticas from './Estatisticas.js';
import MiniPlayer from './MiniPlayer.js';
import { ENDPOINT } from '../GlobalVars.js';
const $  = window.$;

export default class PainelVods extends Component {

  cursorPagina = 0;

  constructor(props) {
    super(props);

    this.state = {
      "vodsIniciaisCarregados": false,
      "carregouTodos": false,
      "carregandoVods": false,
      "telaAtual": "vods",
      "displayVods": 'flex',
      "displayFiltro": 'none',
      "displayNerds": 'none',
      "filtroJogos": [],
      "listaVods": []
    };

    this.listarVods = this.listarVods.bind(this);
    this.receberVods = this.receberVods.bind(this);
    this.paginarVods = this.paginarVods.bind(this);
    this.aplicarFiltroJogos = this.aplicarFiltroJogos.bind(this);
  }

  componentDidMount() {this.paginarVods();}

  paginarVods() {
    if(this.state.carregouTodos) return false;
    this.listarVods();
    return true;
  }

  receberVods(novos) {
    if(!this.state.vodsIniciaisCarregados)
      this.setState({"vodsIniciaisCarregados": true});

    if(!Array.isArray(novos)) return false;

    if(novos.length < 10) // A API sempre retorna 10, a não ser que tenham acabado
      this.setState({"carregouTodos": true});

    Array.prototype.push.apply(this.state.listaVods, novos)
    const novo = this.state.listaVods.slice(0);
    this.setState({'listaVods': novo});

    this.cursorPagina++;
  }

  listarVods() {
    if(this.state.carregandoVods) return false;
    this.setState({'carregandoVods': true});

    $.ajax({
             url: ENDPOINT+"/vods/?idUser=" +
                                            this.props.idUser +
                                            "&pagina=" + this.cursorPagina +
                                            '&manterMomentos',
             type: "GET",
             success: (r) => {
              this.receberVods(r);
              this.setState({'carregandoVods': false});
             },
             error: () => {this.setState({'carregandoVods': false});}
          });
  }

  paginacao() {
    const livesStyle = {'cursor': 'pointer', 'ativo': ''};
    const filtroStyle = {'cursor': 'pointer', 'ativo': ''};
    const nerdsStyle = {'cursor': 'pointer', 'ativo': ''};

    if(this.state.telaAtual === 'vods') {
      livesStyle.cursor = 'inherit';
      livesStyle.ativo = ' active';
    }
    if(this.state.telaAtual === 'filtro') {
      filtroStyle.cursor = 'inherit';
      filtroStyle.ativo = ' active';
    }
    if(this.state.telaAtual === 'nerds') {
      nerdsStyle.cursor = 'inherit';
      nerdsStyle.ativo = ' active';
    }

    return (
      <ul className="nav nav-tabs" style={{'marginBottom': '10px'}}>
        <li className="nav-item">
          <button className={"nav-link"+livesStyle.ativo} style={livesStyle} onClick={() => {this.trocarPainel('vods');}}>Lives</button>
        </li>
        <li className="nav-item ml-1">
          <button className={"nav-link"+filtroStyle.ativo} style={filtroStyle} onClick={() => {this.trocarPainel('filtro');}}>Filtrar por jogos</button>
        </li>
        <li className="nav-item ml-1">
          <button className={"nav-link"+nerdsStyle.ativo} style={nerdsStyle} onClick={() => {this.trocarPainel('nerds');}}>Sou Nerd</button>
        </li>
        <li className="nav-item ml-auto">
          <button key={-99} type="button" className="btn btn-outline-success"
            onClick={() => {
              window.scrollTo(0,document.body.scrollHeight);
            }}
          >
            <span className="fa fa-angle-down" style={{'fontSize': '16pt'}}></span>
          </button>
        </li>
      </ul>
    );
  }

  trocarPainel(qual) {
    if(qual === 'vods') {
      this.setState({
                      'telaAtual': 'vods',
                      'displayVods': 'flex',
                      'displayFiltro': 'none',
                      'displayNerds': 'none'
                    });
    }
    if(qual === 'filtro') {
      this.setState({
                      'telaAtual': 'filtro',
                      'displayVods': 'none',
                      'displayFiltro': 'flex',
                      'displayNerds': 'none'
                    });
    }
    if(qual === 'nerds') {
      this.setState({
                      'telaAtual': 'nerds',
                      'displayVods': 'none',
                      'displayFiltro': 'none',
                      'displayNerds': 'flex'
                    });
    }
  }

  formatarVodsEmMiniPlayers(lista) {

    if(this.state.filtroJogos.length > 0) {
      lista = lista.filter((vod) => {
        for(let i = 0; i < vod.linhaDoTempo.length; i++) {
          if(this.state.filtroJogos.includes(vod.linhaDoTempo[i].game_id))
            return true;
        }
        return false;
      });
    }

    lista.sort((a, b) => { // Ordenando do maior para o menor(inverso)
      if(a.started_at > b.started_at) return -1;
      if(a.started_at < b.started_at) return 1;
      return 0;
    });

    let agoraTimestamp = Math.floor((new Date()).getTime() / 1000);

    const setores = [
      {
        'nome': 'Últimas 24h',
        'jaFoi': false,
        'inicio': agoraTimestamp,
        'fim': agoraTimestamp - (24*60*60)
      },
      {
        'nome': 'Ontem',
        'jaFoi': false,
        'inicio': agoraTimestamp - ((24*60*60) +1),
        'fim': agoraTimestamp - (2*24*60*60)
      },
      {
        'nome': 'Essa semana',
        'jaFoi': false,
        'inicio': agoraTimestamp - ((2*24*60*60) +1),
        'fim': agoraTimestamp - (7*24*60*60)
      },
      {
        'nome': 'Mais antigos',
        'jaFoi': false,
        'inicio': agoraTimestamp - ((7*24*60*60) +1),
        'fim': -Infinity
      },
    ];

    let listaComponentes = [];
    let indexSetores = 0;
    for(let i = 0; i < lista.length; i++) {

      while(indexSetores < setores.length && (lista[i].started_at > setores[indexSetores].inicio || lista[i].started_at < setores[indexSetores].fim))
        indexSetores++;

      if(setores[indexSetores] !== undefined && !setores[indexSetores].jaFoi) {
        listaComponentes.push(
            <h4 key={-indexSetores} className="divisorVods">{setores[indexSetores].nome}</h4>
        );
        setores[indexSetores].jaFoi = true;
      }

      listaComponentes.push(
        <MiniPlayer
                    key={lista[i].id}
                    id={lista[i].id}
                    thumbnail_url={lista[i].thumbnail_url}
                    duration={lista[i].duration}
                    title={lista[i].title}
        />
      );

    }

    return listaComponentes;
  }

  aplicarFiltroJogos(quais) {this.setState({'filtroJogos': quais});}

  render() {
    let listaVods = <h1>Não pude localizar VODs desse canal <img src="https://static-cdn.jtvnw.net/emoticons/v1/86/1.0" alt="BibleThump" /></h1>;

    if(!this.state.vodsIniciaisCarregados) {
      listaVods = [<LoadingCardVod key={0} />, <LoadingCardVod key={1} />, <LoadingCardVod key={2} />, <LoadingCardVod key={3} />];
    }
    else {
      if(this.state.listaVods.length > 0) {

        listaVods = this.formatarVodsEmMiniPlayers(this.state.listaVods);

        //Se já existem vods prontos, mas está carregando mais
        if(this.state.carregandoVods)
          listaVods.push(<LoadingCardVod key={-100} />);
      }
    }

    let bntCarregarMais = <button type="button"
                                  className="btn btn-outline-primary"
                                  style={{"marginBottom": "20px"}}
                                  onClick={this.paginarVods}>Carregar mais</button>;

    if(this.state.carregandoVods)
      bntCarregarMais = <button type="button" className="btn btn-outline-primary" style={{"marginBottom": "20px"}} disabled>Carregando...</button>;
    if(this.state.carregouTodos)
      bntCarregarMais = <button type="button" className="btn btn-outline-secondary" style={{"marginBottom": "20px"}} disabled>Acabou :(</button>;
        return (
          <div>
            <div className="row" style={{'display': this.state.displayVods}}>
              <div className="col-12">
                {this.paginacao()}
                {listaVods}
              </div>
            </div>
            <hr style={{'display': this.state.displayVods}} />
            <div className="row" style={{'display': this.state.displayVods}}>
              <div className="col-5 offset-5 col-md-2 offset-md-8">
                {bntCarregarMais}
              </div>
              <div className="col-2">
                <button type="button" className="btn btn-outline-success ml-auto d-block"
                  onClick={() => {
                    document.body.scrollTop = 0; // For Safari
                    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                  }}
                >
                  <span className="fa fa-angle-up" style={{'fontSize': '16pt'}}></span>
                </button>
              </div>
            </div>
            <div className="row" style={{'display': this.state.displayFiltro}}>
              <div className="col-12">
                {this.paginacao()}
                <FiltroVODPorJogos endpoint={ENDPOINT} listaVods={this.state.listaVods} aplicado={this.aplicarFiltroJogos} />
              </div>
            </div>
            <div className="row" style={{'display': this.state.displayNerds}}>
              <div className="col-12">
                {this.paginacao()}
                <Estatisticas endpoint={ENDPOINT} listaVods={this.state.listaVods} />
              </div>
            </div>
          </div>
        );
  }
}
