import React, { Component } from 'react';

export default class LoadingCardJogo extends Component {
  render() {
    return (
      <div className="loadingComponent loadingCardJogo">
          <div className="animated-background" style={{'backgroundSize': '800px 250px', 'height': '250px'}}>
              <div className="background-masker" style={{'top': '140px', 'left': '0px', 'right': '0px', 'height': '20px'}}></div>
              <div className="background-masker" style={{'top': '160px', 'left': '0px', 'width': '5px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '160px', 'right': '0px', 'width': '5px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '170px', 'left': '0px', 'right': '0px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '180px', 'left': '0px', 'width': '5px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '180px', 'right': '0px', 'width': '5px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '190px', 'left': '0px', 'right': '0px', 'height': '20px'}}></div>
              <div className="background-masker" style={{'top': '210px', 'left': '0px', 'width': '15px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '210px', 'right': '0px', 'width': '15px', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '220px', 'left': '0px', 'right': '0px', 'height': '29px'}}></div>
          </div>
      </div>
    );
  }
}
