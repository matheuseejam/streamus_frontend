import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { LIMITE_ASSISTIR_MAIS_TARDE } from '../GlobalVars';

export default class MiniPlayer extends Component {

  intervalo = null;

  constructor(props) {
    super(props);

    this.state = {
      assistirMaisTardeImg: "imgs/depois.png",
      assistirMaisTardeLista: []
    };

    this.mouseOutAssistirMaisTarde = this.mouseOutAssistirMaisTarde.bind(this);
    this.mouseOverAssistirMaisTarde = this.mouseOverAssistirMaisTarde.bind(this);
    this.addAssistirMaistarde = this.addAssistirMaistarde.bind(this);
    this.removeAssistirMaistarde = this.removeAssistirMaistarde.bind(this);
    this.getListaAssistirMaisTarde = this.getListaAssistirMaisTarde.bind(this);
  }

  componentDidMount() {this.intervalo = setInterval(() => {this.setState({'assistirMaisTardeLista': this.getListaAssistirMaisTarde()});}, 1000);}
  componentWillUnmount() {clearInterval(this.intervalo);}

  estilos = {
    container: {
                "marginLeft": "10px",
                "width": "300px",
                "height": "250px",
                "display": "inline-block",
                "overflow": "hidden"
              },
    divPlay: {
      'height': '135px',
      'width': '290px',
      'position': 'relative',
      'top': '-70px',
      'opacity': 0
    },
    thumbnail: {"cursor": "pointer"},
    painelControles: {
                "width": "290px",
                "height": "165px",
                "backgroundColor": "black",
                "position": "relative",
                "top": "-170px",
                "left": "5px",
                "cursor": "pointer"
              },
    titulo: {"position": "relative", "top": "-165px"},
    controles: {
      play: {
        "color": "white",
        "fontSize": "100px",
        "position": "relative",
        "left": "120px",
        "top": "35px"
      },
      maisTarde: {
        "position": "relative",
        "left": "175px",
        "top": "-65px",
        "width": "30px"
      },
      maisTardeOK: {
        "position": "relative",
        "left": "175px",
        "top": "-60px",
        "color": "white",
        "fontSize": "25px"
      },
      duration: {
        "position": "relative",
        "left": "-100px",
        "top": "-65px",
        "color": "white",
        "fontSize": "15px",
        "textDecoration": "underline"
      }
    }
  };

  addAssistirMaistarde() {
    let vetor = this.getListaAssistirMaisTarde();

    if(vetor.length >= LIMITE_ASSISTIR_MAIS_TARDE ) return false;

    vetor.push(this.props.id);
    this.setCookie('assistirMaisTarde', JSON.stringify(vetor), 365);
    this.setState({'assistirMaisTardeLista': vetor});
  }

  removeAssistirMaistarde() {
    let vetor = this.getListaAssistirMaisTarde();

    let index = vetor.indexOf(this.props.id);
    if (index > -1)
      vetor.splice(index, 1);

    this.setCookie('assistirMaisTarde', JSON.stringify(vetor), 365);
    this.setState({'assistirMaisTardeLista': vetor});
  }

  getListaAssistirMaisTarde() {
    let cookie = this.getCookie("assistirMaisTarde");
    let vetor = [];

    if(cookie !== "") {
      try {vetor = JSON.parse(cookie);}
      catch(e) {/* NÃO FAÇA NADA */}
    }

    if(!Array.isArray(vetor))
      vetor = [];

    return vetor;
  }

 mouseOverAssistirMaisTarde() {this.setState({assistirMaisTardeImg: "imgs/depois.gif"});}
 mouseOutAssistirMaisTarde() {this.setState({assistirMaisTardeImg: "imgs/depois.png"});}

  render() {
    let duration = <b><span style={this.estilos.controles.duration}>{this.formatarDuration(this.props.duration)}</span></b>;

    let divPlay = <Link to={"/videos/"+this.props.id}><div style={this.estilos.divPlay}></div></Link>

    let amt = <img src={this.state.assistirMaisTardeImg} alt="Assistir mais tarde" style={this.estilos.controles.maisTarde} onMouseOver={this.mouseOverAssistirMaisTarde} onMouseOut={this.mouseOutAssistirMaisTarde} onClick={this.addAssistirMaistarde} />;
    if(this.state.assistirMaisTardeLista.includes(this.props.id))
      amt = <span className="fa fa-check" style={this.estilos.controles.maisTardeOK} onClick={this.removeAssistirMaistarde}></span>;

    return (
      <div style={this.estilos.container}>
        <img src={this.props.thumbnail_url.replace(/%\{width\}/, "300").replace(/%\{height\}/, "170")} className="img-thumbnail" alt="Thumb do VOD" style={this.estilos.thumbnail} />
        <div style={this.estilos.painelControles} className="thumbControlVod">
          <span className="fa fa-play" style={this.estilos.controles.play}></span>
          {amt}
          {duration}
          {divPlay}
        </div>
        <p style={this.estilos.titulo}>{this.props.title}</p>
      </div>
    );
  }

  formatarDuration(original) {
    original = original.split('s')[0]; //Eliminando s desnescesário
    let vetor = original.split(/(h|m)/);

    let hora = null,
        minuto = null,
        segundo = null;

    if(vetor.length === 5) {
      if(vetor[0].length > 1) hora = vetor[0];
      else             hora = '0'+vetor[0];

      if(vetor[2].length > 1) minuto = vetor[2];
      else             minuto = '0'+vetor[2];

      if(vetor[4].length > 1) segundo = vetor[4];
      else                    segundo = '0'+vetor[4];
    }
    else if(vetor.length === 3) {
      if(vetor[0].length > 1) minuto = vetor[0];
      else             minuto = '0'+vetor[0];

      if(vetor[2].length > 1) segundo = vetor[2];
      else                    segundo = '0'+vetor[2];
    }
    else if(vetor.length === 1) {
      if(vetor[0].length > 1) segundo = vetor[0];
      else                    segundo = '0'+vetor[0];
    }

    if(hora !== null) return hora+':'+minuto+':'+segundo;
    else if(minuto !== null) return minuto+':'+segundo;
    else return segundo+'s';
  }

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
}
