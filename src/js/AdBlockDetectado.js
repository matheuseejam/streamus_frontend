import React, { Component } from 'react';

export default class AdBlockDetectado extends Component {

  render() {
    return (
      <div className="row">
        <div className="col-6 offset-3">
          <div className="card text-center" style={{"marginTop": "50px"}}>
            <div className="card-header">
              Bloqueio do bloqueio
            </div>
            <div className="card-body">
              <h5 className="card-title">Ops, Parece que você está usando AdBlock</h5>
              <p className="card-text">Infelizmente este sistema é mantido apenas por estudantes de tecnologia, e, como você sabe, <b>estudante não tem dinheiro</b>. Por favor, desabilite o AdBlock para que possamos ao menos pagar os servidores.</p>
            </div>
            <div className="card-footer text-muted">
              Desabilite o AdBlock e reinicie a página.
            </div>
          </div>
        </div>
      </div>
    );
  }
}
