import React, { Component } from 'react';
import Cabecalho from './js/Cabecalho.js';
import PainelPrincipal from './js/PainelPrincipal.js'
import PrincipaisJogos from './js/PrincipaisJogos.js'
import PropagandaLinha from './js/PropagandaLinha.js'
import Streamers from './js/Streamers.js'
import AdBlockDetectado from './js/AdBlockDetectado.js';
import {ENDPOINT} from './js/GlobalVars.js';

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      'adBlock': false
    };

    this.adBloqueado = this.adBloqueado.bind(this);
  }

  componentDidMount() {document.title = 'StreamUs';}

  adBloqueado() {
    this.setState({'adBlock': true});
  }

  render() {
    if(this.state.adBlock)
      return <AdBlockDetectado />;

    return (
      <div>
        <Cabecalho />
        <div className="container-fluid" id="containerCorpo">
          <div className="row">
            <Streamers endpoint={ENDPOINT} />
            <div className="col-sm-12 col-md-10">
              <PainelPrincipal endpoint={ENDPOINT} />
              <hr />
              <PropagandaLinha bloqueado={this.adBloqueado} />
              <PrincipaisJogos endpoint={ENDPOINT} />
            </div>
          </div>
          <div className="row">
            <div className='col-8 ml-auto' style={{'textAlign': 'right'}}>
              <span>Desenvolvido por <a target="_blank" rel="noopener noreferrer" href='https://matheusalves.com.br'>Teteuzinho</a></span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
