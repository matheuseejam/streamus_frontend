import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class Streamer extends Component {
  render() {
    return (
      <Link to={"/"+this.props.nomeCanal} style={{"color": "inherit"}}>
        <div className="cardStreamer">
          <img src={this.props.profileImage} alt="Imagem perfil do streamer" />
          <div>
            <h5 className="card-title">{this.props.nomeCanal}</h5>
            <h6 className="card-title">{this.props.nViews} Views</h6>
          </div>
        </div>
      </Link>
    );
  }
}
