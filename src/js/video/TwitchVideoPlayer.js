import React, { Component } from 'react';
const Twitch  = window.Twitch;

const $ = window.$;

export default class TwitchVideoPlayer extends Component {

  player = null;
  eventosAdicionados = false;

  componentDidUpdate() {
    if(typeof this.props.idVideo !== 'string') return false;

    const options = {
          'width': this.props.width,
          'height': this.props.height,
          'video': this.props.idVideo
        };

    $('#twitchVideoPlayer').empty();
    this.player = new Twitch.Player("twitchVideoPlayer", options);

    if(this.props !== undefined && this.props !== null) {

      if(!this.eventosAdicionados) {
        if(this.props.onPlaying !== undefined || this.props.onPaused !== undefined) {

          this.player.addEventListener(Twitch.Player.READY, () => {
            this.eventosAdicionados = true;

            if(this.props.onPlaying !== undefined) {
              this.player.addEventListener(Twitch.Player.PLAYING, () => {this.props.onPlaying();});
            }
            if(this.props.onPaused !== undefined) {
              this.player.addEventListener(Twitch.Player.PAUSE, () => {this.props.onPaused();});
              this.player.addEventListener(Twitch.Player.ENDED, () => {this.props.onPaused();});
            }

          });

        }

      }

      this.props.returnVideoControler(this.player);
    }

  }

  render() {
    return ( <div id="twitchVideoPlayer"></div> );
  }
}
