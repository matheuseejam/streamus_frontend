import React, { Component } from 'react';
import ChatTwitchLive from './js/live/ChatTwitchLive.js';
import { ENDPOINT } from './js/GlobalVars';
const $ = window.$;

export default class Live extends Component {

  constructor(props) {
    super(props);

    this.state = {'idStreamer': 0};
  }

  componentDidMount() {
    document.body.classList.add('videoContainerExterno');

    let listaCaminhos = window.location.pathname.split('/');
    let nome = listaCaminhos[listaCaminhos.length - 1]; // Último

    $.ajax({
               url: ENDPOINT+'/userByName/'+nome.toLowerCase(),
               type: "GET",
               success: (r) => {
                 this.setState({'idStreamer': r.id});
               },
               error: () => {/* Empty */}
            });
  }

  render() {
    let listaCaminhos = window.location.pathname.split('/');
    let nome = listaCaminhos[listaCaminhos.length - 1]; // Último

    return (
      <div className="container-fluid" style={{'maxWidth': '1600px'}}>
        <div className="row">
          <div className="col-12 col-md-9" id="containerVideo">
            <div className="row">

              <div className="col-12" id="containerVideo">
                <div className="embed-responsive embed-responsive-16by9">
                  <iframe
                      title="Stream"
                      src={"https://player.twitch.tv/?channel="+nome+'&muted=false'}
                      height="0"
                      width="0"
                      frameBorder="0"
                      scrolling="no"
                      allowFullScreen={true}>
                  </iframe>
                </div>
              </div>

            </div>

            <div className="row">

              <div className="col-12 d-none d-md-block" style={{
                  'marginTop': '10px',
                  'marginBottom': '10px'
                }}>
                <a href={'/'+nome}>
                  <button type="button" className="btn btn-outline-light">
                    <span className="fa fa-arrow-left"></span>  {nome}
                  </button>
                </a>
              </div>

            </div>

          </div>
          <div className="col-12 col-md-3">
            <ChatTwitchLive nomeStreamer={nome.toLowerCase()} idStreamer={this.state.idStreamer} />
          </div>
        </div>
      </div>
      );
  }

}
