import React, { Component } from 'react';
import { AreaChart, Area, Brush, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,
        BarChart, Bar, Legend
} from 'recharts';
const $  = window.$;

export default class Estatisticas extends Component {

  hash = null;
  hashJogos = null;
  listaJogosExterna = [];
  cacheJogos = [];

  constructor(props) {
    super(props);
    this.state = {
      'listaVods': [],
      'listaJogos': []
    };
  }

  componentDidUpdate() {
    this.processarVODs();
    this.processarJogos();
  }

  processarVODs() {
    const v = this.props.listaVods;
    if(JSON.stringify(v) === this.hash) return false;
    else this.hash = JSON.stringify(v);

    const processado = v.map((vod) => {

      const r = {'timestamp': vod.started_at, 'media': 0};

      for(let i = 0; i < vod.linhaDoTempo.length; i++)
        r.media += vod.linhaDoTempo[i].viewer_count;
      r.media = Math.round(r.media / vod.linhaDoTempo.length);

      return r;
    });

    processado.sort((a, b) => {
      if(a.timestamp > b.timestamp) return 1;
      if(a.timestamp < b.timestamp) return -1;
      return 0;
    });

    const final = processado.map((vod) => {
      let d = new Date(vod.timestamp*1000);
      vod.data = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear();
      return vod;
    });

    this.setState({'listaVods': final});
  }

  processarJogos() {
    const v = this.props.listaVods;
    if(JSON.stringify(v) === this.hashJogos) return false;
    else this.hashJogos = JSON.stringify(v);

    let listaFinal = [];
    for (var i = 0; i < v.length; i++) {

      let VOD = v[i];
      let jogoAtual = null;
      let soma = 0;
      let nMomentos = 0;
      let listaJogos = [];
      for (var j = 0; j < VOD.linhaDoTempo.length; j++) {
        let momento = VOD.linhaDoTempo[j];

        if(jogoAtual !== momento.game_id) { // O jogo mudou nesse momento da live
          let indexJogo = this.getIndexJogo(jogoAtual, listaJogos);
          if(indexJogo !== -1) {
            if(listaJogos[indexJogo].media < Math.floor(soma/nMomentos))
              listaJogos[indexJogo].media = Math.floor(soma/nMomentos);
          }
          else if(jogoAtual !== null) {
            listaJogos.push({
              'id': jogoAtual,
              'media': Math.floor(soma/nMomentos)
            });
          }

          jogoAtual = momento.game_id;
          soma = momento.viewer_count;
          nMomentos = 1;
        }
        else {
          soma += momento.viewer_count;
          nMomentos++;
          // O VOD atual terminou de ser lido
          // por isso o jogo não será inserido na próxima iteração
          if(j+1 >= VOD.linhaDoTempo.length) {
            listaJogos.push({
              'id': jogoAtual,
              'media': Math.floor(soma/nMomentos)
            });
          }
        }
      }

      // Lista de jogos dessa live concluida, mesclando com a lista final
      for (var loop = 0; loop < listaJogos.length; loop++) {
        let index = this.getIndexJogo(listaJogos[loop].id, listaFinal);
        if(index !== -1) {
          if(listaFinal[index].media < listaJogos[loop].media)
            listaFinal[index].media = listaJogos[loop].media;
        }
        else
          listaFinal.push(listaJogos[loop]);
      }

    }

    listaFinal = listaFinal.sort((a, b) => {
      if(a.media > b.media) return -1;
      if(a.media < b.media) return 1;
      return 0;
    });

    this.buscarInfoJogos(listaFinal.slice(0, 5));
  }

  getIndexJogo(id, lista) {
    for (var i = 0; i < lista.length; i++)
      if(lista[i].id === id)
        return i;

    return -1;
  }

  addCache(novo) {
    for(let i = 0; i < this.cacheJogos.length; i++)
      if(this.cacheJogos[i].id === novo.id)
        return false;

    this.cacheJogos.push(novo);
    return true;
  }

  getCache(id) {
    for(let i = 0; i < this.cacheJogos.length; i++)
      if(this.cacheJogos[i].id === id)
        return this.cacheJogos[i];

    return null;
  }

  buscarInfoJogos(lista) {
    this.listaJogosExterna = [];
    this.setState({'listaJogos': []});

    for (var i = 0; i < lista.length; i++) {
      let jogo = lista[i];

      if(this.getCache(jogo.id) !== null) {
        const novo = this.listaJogosExterna.slice(0);
        novo.push({
          'id': jogo.id,
          'media': jogo.media,
          'nome': this.getCache(jogo.id).name
        });
        this.listaJogosExterna = novo;
        this.setState({'listaJogos': novo});

        continue;
      }

      $.ajax({
               url: this.props.endpoint+"/jogo/"+jogo.id,
               type: "GET",
               success: (r) => {
                 const novo = this.listaJogosExterna.slice(0);
                 novo.push({
                   'id': jogo.id,
                   'media': jogo.media,
                   'nome': r.name
                 });
                 this.listaJogosExterna = novo;
                 this.setState({'listaJogos': novo});
                 this.addCache(r);
               },
               error: () => {/* Empty */}
            });
    }
  }

  render() {
    const listaJogos = this.state.listaJogos.sort(
      (a, b) => {
        if(a.media > b.media) return -1;
        if(a.media < b.media) return 1;
        return 0;
      }
    );
    return (
      <section className="row">
        <div className="col-12">
          <h3 style={{'textAlign': 'right'}}>Estatísticas</h3><hr />
          <div className="corpoPainel">
            <h4>Média de visualizações a cada live</h4>
            <ResponsiveContainer width="100%" height={300}>
              <AreaChart
                        data={this.state.listaVods}
                        margin={{top: 10, right: 30, left: 0, bottom: 0}}
              >
                <Brush />
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey='data' />
                <YAxis />
                <Tooltip />
                <Area type='monotone' dataKey='media' stroke='#82ca9d' fill='#82ca9d' />
              </AreaChart>
            </ResponsiveContainer>

            <hr />

              <h4>Médias de viewers dos principais jogos</h4>
              <ResponsiveContainer width="100%" height={300}>
                <BarChart data={listaJogos}
                      margin={{top: 10, right: 30, left: 0, bottom: 0}}>
                 <CartesianGrid strokeDasharray="3 3"/>
                 <XAxis dataKey="nome"/>
                 <YAxis/>
                 <Tooltip/>
                 <Legend />
                 <Bar dataKey="media" fill="#8884d8" />
                </BarChart>
              </ResponsiveContainer>

          </div>
        </div>
      </section>
    );
  }

}
