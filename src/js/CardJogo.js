import React, { Component } from 'react';

export default class CardJogo extends Component {
  render() {
    return (
      <figure className="figure cardJogo">
        <img src={this.props.imagem} className="figure-img img-fluid rounded" alt="Imagem de capa do jogo." />
        <figcaption className="figure-caption">{this.props.nome}</figcaption>
        <figcaption className="figure-caption">{this.props.nLives} Lives</figcaption>
      </figure>
    );
  }
}
