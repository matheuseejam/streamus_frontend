import React, { Component } from 'react';
import Streamer from './Streamer.js';
import LoadingStreamer from './loadingComponents/LoadingStreamer.js';
const $ = window.$;

export default class Streamers extends Component {

  constructor(props) {
    super(props);
    this.state = {
      'recastErro': false,
      'listaStreamers': []
    };

    this.iniciarListaStreamers();
  }

  iniciarListaStreamers() {
    this.ajaxRecast(
      this.props.endpoint+"/estatisticas/onlineagora",
      {},
      "GET",
      this.finalizarListarStreamers.bind(this),
      () => {this.setState({'recastErro': true});}
    );
  }

  finalizarListarStreamers(lista) {
    let dados = lista.map((streamer) => {return {nomeCanal: streamer.nome, nViews: streamer.nViews, profileImage: streamer.imagem};});
    this.setState({listaStreamers: dados});
  }

  render() {
    let lista = [
                  <LoadingStreamer key={0} />,
                  <LoadingStreamer key={1} />,
                  <LoadingStreamer key={2} />,
                  <LoadingStreamer key={3} />
                ];

    if(this.state.listaStreamers.length > 0)
      lista = [];

    for(let x = 0; x < this.state.listaStreamers.length; x++) {
      lista.push(<Streamer key={x} nomeCanal={this.state.listaStreamers[x].nomeCanal} nViews={this.state.listaStreamers[x].nViews} profileImage={this.state.listaStreamers[x].profileImage} />)
    }

    if(this.state.recastErro) {
      lista = (
        <div className="alert alert-dark" role="alert">
          Lista indisponível.
        </div>
      );
    }

    return (
      <section className="col-md-2 d-none d-md-block" id="painelLateral">
        <h5>Rolando agora <span id="simboloLive" role="img" aria-label="Ponto vermelho piscando">&#128308;</span></h5>
        {lista}
      </section>
    );
  }

  ajaxRecast(url, data, tipo, callbackSucesso, callbackErro) {
    $.ajax({
             url: url,
             data: data,
             type: tipo,
             success: callbackSucesso,
             error: callbackErro
          });
  }
}
