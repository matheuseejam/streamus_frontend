import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './styles/css/index.css';

import App from './App';
import Streamer from './Streamer';
import Video from './Video';
import Live from './Live';

import * as serviceWorker from './serviceWorker';


ReactDOM.render(
  <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={App} />
            <Route path="/videos/*" exact={true} component={Video} />
            <Route path="/live/*" exact={true} component={Live} />
            <Route path="*" component={Streamer} />
        </Switch>
    </ BrowserRouter>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
