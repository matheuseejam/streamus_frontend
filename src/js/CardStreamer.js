import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  LineChart,
  Line,
  CartesianGrid,
  Tooltip,
} from 'recharts';

export default class CardStreamer extends Component {

  constructor(props) {
    super(props);

    this.testarImagemDesatualizada.bind(this);
  }

  componentDidMount() {
    this.testarImagemDesatualizada(this.props.imagem);
  }

  testarImagemDesatualizada(url) {
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = (e) => {
        if( xhr.readyState === 4 ) {
          if (xhr.status === 200) {
            if (url !== xhr.responseURL)
              this.alertarUserDesatualizado();
          }
          else this.alertarUserDesatualizado();
        }
      };
      xhr.open("GET", url, true);
      xhr.send();
  }

  alertarUserDesatualizado() {
    var xhr = new XMLHttpRequest();
    xhr.open("PATCH", this.props.endpoint+'/userByName/'+this.props.nome, true);
    xhr.send();
  }

  render() {
    return (
      <div className="cardPainelStreamer">
        <div className="infosCardPainelStreamer">
          <img src={this.props.imagem} className="imgCardPainelStreamer rounded" alt="Streamer" />
          <Link to={"/"+this.props.nome} style={{"color": "inherit"}}><h4>{this.props.nome}</h4></Link>
        </div>
        <div className="graficoCardPainelStreamer">
          <LineChart
            width={this.props.dimencoes.x}
            height={this.props.dimencoes.y}
            data={this.props.dados}
            margin={{top: 5, right: 30, left: 20, bottom: 5}}
            >
            <Line
              type='monotone'
              dataKey='Viewers'
              stroke='#8884d8'
              activeDot={{r: 8}}
              />
            <CartesianGrid strokeDasharray='3 3'/>
            <Tooltip/>
          </LineChart>
        </div>
      </div>
    );
  }
}
