import React, { Component } from 'react';
import MensagemChatTwitchVOD from '../video/MensagemChatTwitchVOD.js'
import { ENDPOINT } from '../GlobalVars';
const $ = window.$;

export default class ChatTwitchLive extends Component {

  chatDeveScrollar = true;
  lastTouchScreenY = Infinity;
  mouseNoChat = false;
  mensagensNaTela = [];
  buffer = [];
  BTTVEmotes = new Map();
  TwitchBadges = new Map();
  con = null;
  timeout = null;

  style = {
    'container': {
                    'position': 'fixed',
                    'height': '100vh',
                    'backgroundColor': '#232526',
                    'maxWidth': '500px',
                    'minWidth': '24%'
                  },
    'containerPortrait': {
                    'height': '70vh',
                    'backgroundColor': '#232526',
                    'minWidth': '100%'
                  },
    'containerMensagens': {
                    'overflow': 'auto',
                    'height': '100%'
                  },
    'head': {
      'position': 'absolute',
      'top': '0px',
      'borderBottom': '1px solid white',
      'backgroundColor': 'black',
      'height': '30px',
      'width': '100%'
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      'mensagensNaTela': [],
      'styleContainer': 'container'
     }

    this.preencherChat = this.preencherChat.bind(this);
    this.movendoChatComDedo = this.movendoChatComDedo.bind(this);
    this.iniciarConexao = this.iniciarConexao.bind(this);
    this.receber = this.receber.bind(this);
    this.erroConexao = this.erroConexao.bind(this);
    this.parse = this.parse.bind(this);
  }

  componentDidMount() {
    this.preencherChat();

    this.carregarBTTVEmotes();
    this.carregarBadgesTwitch();

    // Evento nescessário para detectar quando o usuário está tentando
    // Ver mensagens antigas no chat
    window.addEventListener("wheel",
      (event) => {
        if(event.deltaY < 0 && this.mouseNoChat)
          this.chatDeveScrollar = false;
      },
      {
        capture: true,
        passive: true
      }
    );

    this.iniciarConexao();
  }
  componentWillUnmount() {
    this.con.close();
    clearTimeout(this.timeout);
  }

  iniciarConexao() {
    this.con = new WebSocket('wss://irc-ws.chat.twitch.tv/');

    this.con.onopen = () => {
      this.con.send('CAP REQ :twitch.tv/tags twitch.tv/commands');
      this.con.send('PASS SCHMOOPIIE');
      this.con.send('NICK justinfan0777787773326887777');
      this.con.send('JOIN #'+this.props.nomeStreamer);
    };
    this.con.onerror = this.erroConexao;
    this.con.onmessage = this.receber;
  }

  receber(mensagemEvento) {
    let mensagem = mensagemEvento.data;
    if(mensagem.slice(0,1) !== '@') {
      if(mensagem.slice(0,4) === 'PING') {
        this.con.send('PONG :tmi.twitch.tv');
      }

      return;
    }

    let dados = this.parse(mensagem);
    if(dados.get('user-type')) {
      let mensagemJson = this.map2Json(dados);
      this.buffer.push(mensagemJson);
    }
  }

  erroConexao(erro) {this.iniciarConexao();}

  parse(bruto) {
    bruto = bruto.slice(1);
    let r = new Map();
    let linhas = bruto.split(';');
    for (var i = 0; i < linhas.length; i++) {
      let linha = linhas[i];
      let chave_valor = linha.split('=');
      if(chave_valor[1] !== '')
        if(chave_valor[0] !== 'user-type')
          r.set(chave_valor[0], chave_valor[1]);
        else {
          let valor = chave_valor[1];

          let afterPRIVMSG = valor.slice(valor.indexOf('PRIVMSG #'+this.props.nomeStreamer.toLowerCase()));
          let afterPoints = afterPRIVMSG.slice(afterPRIVMSG.indexOf(':')+1);
          let semNL = afterPoints.replace(/([\n]|[\r\n])/g, '');

          r.set(chave_valor[0], semNL);
        }

    }


    return r;
  }

  map2Json(map) {
    let r = {
      'message': {
        'fragments': []
      },
      'commenter': {},
    };

    if(map.get('id')) r._id = map.get('id');
    if(map.get('display-name')) r.commenter.display_name = map.get('display-name');
    if(map.get('badges')) {
      let lista = map.get('badges').split(',');
      for (var i = 0; i < lista.length; i++) {

        if(r.message.user_badges === undefined) {
          r.message.user_badges = [{
            '_id': lista[i].split('/')[0],
            'version': lista[i].split('/')[1]
          }];
        }
        else {
          r.message.user_badges.push({
            '_id': lista[i].split('/')[0],
            'version': lista[i].split('/')[1]
          });
        }

      }
    }
    if(map.get('color')) r.message.user_color = map.get('color');

    let msg = map.get('user-type');
    if(map.get('emotes') === undefined) {
      r.message.fragments.push({
        'text': msg
      });
    }
    else {
      let emotes = map.get('emotes').split('/').map((emote) => {
        return {
          'cod': emote.split(':')[0],
          'locais': emote.split(':')[1].split(',').map((range) => {
            return {
              'inicio': Number(range.split('-')[0]),
              'fim': Number(range.split('-')[1])
            }
          })
        };
      });

      let fragmento = {'text': ''};
      let estaDentroDeUmEmote = false;
      for (var cursor = 0; cursor < msg.length; cursor++) {
        if(msg.charCodeAt(cursor) === 1) msg[cursor] = '';

        if(this.ehInicioEmote(cursor, emotes)) {
          fragmento = {
            'text': msg[cursor],
            'emoticon': {
              'emoticon_id': this.ehInicioEmote(cursor, emotes)
            }
          };
          estaDentroDeUmEmote = true;
        }
        else if(this.ehFimEmote(cursor, emotes)) {
          fragmento.text += msg[cursor];
          r.message.fragments.push(fragmento);
          estaDentroDeUmEmote = false;
          fragmento = {'text': ''}; //Iniciando um fragmento sem emote by default
        }
        else {
          fragmento.text += msg[cursor];
          if(!estaDentroDeUmEmote) {
            //Checando se é o fim de um fragmento sem emote
            if((cursor+1) >= msg.length || this.ehInicioEmote(cursor+1, emotes)) {
              r.message.fragments.push(fragmento);
            }
          }
        }

      }

    }

    return r;
  }

  ehInicioEmote(cursor, lista) {
    for (var i = 0; i < lista.length; i++) {

      let locais = lista[i].locais;

      for (var j = 0; j < locais.length; j++)
        if(cursor === locais[j].inicio)
          return lista[i].cod;

    }

    return false;
  }

  ehFimEmote(cursor, lista) {
    for (var i = 0; i < lista.length; i++) {

      let locais = lista[i].locais;

      for (var j = 0; j < locais.length; j++)
        if(cursor === locais[j].fim)
          return lista[i].cod;

    }

    return false;
  }

  movendoChatComDedo(evento) {
    const screenY = evento.changedTouches[0].pageY;
    if(screenY > this.lastTouchScreenY) this.chatDeveScrollar = false;
    this.lastTouchScreenY = screenY;
  }

  preencherChat() {
    let temp = this.mensagensNaTela.slice(0).concat(this.buffer);
    if(temp.length > 100) temp = temp.slice(-100);
    this.mensagensNaTela = temp;
    this.buffer = [];

    if(this.chatDeveScrollar) {
      var elem = document.getElementById('chatContainerMensagens');
      if(elem !== null) elem.scrollTop = elem.scrollHeight;
    }

    this.setState({'mensagensNaTela': this.mensagensNaTela});

    const w = window.innerWidth;
    if(w <= 767/*Ponto de quebra entre PC e cel*/) {
      if(this.state.styleContainer !== 'containerPortrait') this.setState({'styleContainer': 'containerPortrait'});
    }
    else {
      if(this.state.styleContainer !== 'container') this.setState({'styleContainer': 'container'});
    }

    this.timeout = setTimeout(this.preencherChat, 100);
  }

  render() {
    let retomarScroll = '';
    if(!this.chatDeveScrollar)
      retomarScroll = <b
                            style={{'color': 'white', 'cursor': 'pointer'}}
                            onClick={() => {this.chatDeveScrollar = true;}}
                        >Retomar rolagem</b>;

    let head = (
      <div style={this.style.head}>
        {retomarScroll}
        <img
              src="../favicon.png"
              alt="StreamUs"
              height="100%"
              style={{'float': 'right'}}
          />
      </div>);

      const mensagens = [];
      for (var i = 0; i < this.state.mensagensNaTela.length; i++)
        mensagens.push(<MensagemChatTwitchVOD
                                              key={this.state.mensagensNaTela[i]._id}
                                              Mensagen={this.state.mensagensNaTela[i]}
                                              Emotes={this.BTTVEmotes}
                                              TwitchBadges={this.TwitchBadges}
                      />);

    return (
      <div className="row no-gutters" style={this.style[this.state.styleContainer]}>
        <div className="col">
          <div id="chatContainerMensagens"
                onMouseEnter={() => {this.mouseNoChat = true;}}
                onMouseLeave={() => {this.mouseNoChat = false;}}
                onTouchMove={this.movendoChatComDedo}
                className="scrollbarPersonalizada"
                style={this.style.containerMensagens}>
            {head}
            {mensagens}
          </div>
        </div>
      </div>
      );
  }

  carregarBadgesTwitch() {
    if(typeof this.props.idStreamer !== 'string') {
      setTimeout(() => {this.carregarBadgesTwitch();}, 1000); // Tente de novo mais tarde
      return false;
    }

    // Consultado API e adicionando ao Map
    $.ajax({
               url: ENDPOINT+'/globalTwitchBadges',
               type: "GET",
               success: (r) => {
                 $.each(r.badge_sets, (chave, valor) => {
                      if(this.TwitchBadges.get(chave) !== undefined) {
                         let tmp = this.TwitchBadges.get(chave);
                         $.each(valor.versions, (chave2, valor2) => {
                           if(tmp[chave2] === undefined) {
                             tmp[chave2] = valor2;
                           }
                         });
                      }
                      else this.TwitchBadges.set(chave, valor.versions);
                  });
               },
               error: () => {/* Empty */}
            });

    $.ajax({
               url: ENDPOINT+'/channelTwitchBadges/'+this.props.idStreamer,
               type: "GET",
               success: (r) => {
                 $.each(r.badge_sets, (chave, valor) => {
                     if(this.TwitchBadges.get(chave) !== undefined) {
                        let tmp = this.TwitchBadges.get(chave);
                        $.each(valor.versions, (chave2, valor2) => {
                          tmp[chave2] = valor2;
                        });
                     }
                     else this.TwitchBadges.set(chave, valor.versions);
                  });
               },
               error: () => {/* Empty */}
            });
  }

  carregarBTTVEmotes() {
    if(typeof this.props.idStreamer !== 'string') {
      setTimeout(() => {this.carregarBTTVEmotes();}, 1000); // Tente de novo mais tarde
      return false;
    }

    // Frankerfacez Global Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/frankerfacez_emotes/global',
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, emote.images['1x']);
                 });
               },
               error: () => {/* Empty */}
            });

    // BTTV Global Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/emotes',
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, 'https://cdn.betterttv.net/emote/'+emote.id+'/1x');
                 });
               },
               error: () => {/* Empty */}
            });

    // Frankerfacez Channel Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/frankerfacez_emotes/channels/'+this.props.idStreamer,
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, emote.images['1x']);
                 });
               },
               error: () => {/* Empty */}
            });

    // BTTV Channel Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/channels/'+this.props.nomeStreamer,
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, 'https://cdn.betterttv.net/emote/'+emote.id+'/1x');
                 });
               },
               error: () => {/* Empty */}
            });

      return true;
  }

}
