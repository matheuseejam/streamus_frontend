import React, { Component } from 'react';

import { TWITCH_CLIENT_ID } from '../GlobalVars.js';
const $  = window.$;

export default class BntLiveAgora extends Component {

  constructor(props) {
    super(props);

    this.state = {'display': 'none'};
  }

  componentDidMount() {
    $.ajax({
             url: 'https://api.twitch.tv/helix/streams?user_id='+this.props.idStreamer,
             beforeSend: (request) => {
                request.setRequestHeader("client-id", TWITCH_CLIENT_ID);
              },
             type: "GET",
             success: (r) => {
               if(r.data && r.data.length > 0) {
                 this.setState({'display': 'block'});
               }
             },
             error: () => {/* Empty */}
          });
  }

  render() {
    return (
      <a href={'/live/'+this.props.nomeStreamer}
        style={{
          'display': this.state.display,
          'position': 'fixed',
          'bottom': '10px',
          'left': '10px'
        }}
      >
        <button type="button" className="btn btn-danger">
          Online Agora
        </button>
      </a>
    );
  }
}
