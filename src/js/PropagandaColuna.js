import React, { Component } from 'react';

export default class PropagandaColuna extends Component {

  intervalo = null;

  constructor(props) {
    super(props);

    this.state = {
      'adBlock': false
    };
  }

  componentDidMount() {
    this.intervalo = setInterval(() => {
      let p = document.getElementById('propagandaColuna');
      if(p && getComputedStyle(p, null).display !== 'none') {
        if(this.state.adBlock) {
          this.setState({'adBlock': false});
          if(this.props.desbloqueado !== undefined && this.props.desbloqueado !== null)
            this.props.desbloqueado();
        }
      }
      else {
        if(!this.state.adBlock) {
          this.setState({'adBlock': true});
          if(this.props.bloqueado !== undefined && this.props.bloqueado !== null)
            this.props.bloqueado();
        }
      }
    }, 1000);
  }

  componentWillUnmount() {clearInterval(this.intervalo);}

  render() {
    return (
      <div className="col-md-2 d-none d-md-block">
        <a href="https://www.vultr.com/?ref=7208367" target="_blank" rel="noopener noreferrer"><img src="https://www.vultr.com/media/banner_4.png" id="propagandaColuna" width="160" height="600"  alt="Vultr" /></a>
      </div>
    );
  }
}
