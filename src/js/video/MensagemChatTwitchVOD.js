import React, { Component } from 'react';

export default class MensagemChatTwitchVOD extends Component {

  style = {
    'containerExterno': {
      'width': '95%',
      'marginLeft': 'auto',
      'marginRight': 'auto',
      'borderRadius': '10px',
      'border': '1px solid grey',
      'marginBottom': '5px',
      'padding': '5px',

    },
    'containerHead': {
      'borderBottom': '1px solid grey',
      'wordBreak': 'break-all'
    },
    'corpoMensagem': {
      'color': 'white',
      'margin': '0'
    },
    'containerMensagem': {
      'wordBreak': 'break-word'
    }
  };

  render() {
    const m = this.props.Mensagen;

    const headMontado = this.montarHead(m);
    const mensagemMontada = this.montarMensagem(m.message.fragments);

    return (
      <div style={this.style.containerExterno}>
        <div style={this.style.containerHead}>
          {headMontado}
        </div>
        <div style={this.style.containerMensagem}>
          {mensagemMontada}
        </div>
      </div>
    );
  }

  montarHead(mensagem) {
    let nameColor = 'white';
    if(mensagem.message.user_color !== undefined) nameColor = mensagem.message.user_color;
    const nome = <b style={{"color": nameColor}}>{mensagem.commenter.display_name}</b>;

    if(mensagem.message.user_badges === undefined) return nome;

    const badges = [];
    for (var i = 0; i < mensagem.message.user_badges.length; i++) {
      let badge = this.props.TwitchBadges.get(mensagem.message.user_badges[i]._id);
      if(badge !== undefined && badge[mensagem.message.user_badges[i].version] !== undefined)
        badges.push(<span key={i}>
                      <img
                        alt={mensagem.message.user_badges[i]._id}
                        src={badge[mensagem.message.user_badges[i].version].image_url_1x}
                      />
                    <span>{' '}</span>
                    </span>
                  );
    }

    return (<span><span>{badges}</span>{nome}</span>);

  }

  montarMensagem(fragmentos) {
    const montado = [];
    for (var i = 0; i < fragmentos.length; i++) {
      if(fragmentos[i].emoticon === undefined) { // Não é um Emote da Twitch

        const partes = fragmentos[i].text.split(' ');
        let temBTTVEmotes = false;
        for (var j = 0; j < partes.length; j++) {
          if(this.props.Emotes.get(partes[j]) !== undefined) { // É um Emote da BTTV
            partes[j] = (<span key={j}>
                          <span>{' '}</span>
                          <img
                            className="emoteNoChat"
                            alt={partes[j]}
                            src={this.props.Emotes.get(partes[j])}
                          />
                        </span>);
            temBTTVEmotes = true;
          }
          else {
            partes[j] = <span key={j}>{' '+partes[j]}</span>;
          }
        }

        if(temBTTVEmotes) montado.push(<span key={i}>{partes}</span>);
        else              montado.push(<span key={i}>{fragmentos[i].text}</span>);

      }
      else
        montado.push(<img
                        className="emoteNoChat"
                        key={i}
                        alt={fragmentos[i].text}
                        src={'https://static-cdn.jtvnw.net/emoticons/v1/'+fragmentos[i].emoticon.emoticon_id+'/1.0'}
                      />);
    }

    return <p style={this.style.corpoMensagem}>{montado}</p>
  }
}
