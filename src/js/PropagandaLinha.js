import React, { Component } from 'react';

export default class PropagandaLinha extends Component {

  intervalo = null;

  constructor(props) {
    super(props);

    this.state = {
      'adBlock': false
    };
  }

  componentDidMount() {
    this.intervalo = setInterval(() => {
      let p = document.getElementById('propagandaLinha');
      if(p && getComputedStyle(p, null).display !== 'none') {
        if(this.state.adBlock) {
          this.setState({'adBlock': false});
          if(this.props.desbloqueado !== undefined && this.props.desbloqueado !== null)
            this.props.desbloqueado();
        }
      }
      else {
        if(!this.state.adBlock) {
          this.setState({'adBlock': true});
          if(this.props.bloqueado !== undefined && this.props.bloqueado !== null)
            this.props.bloqueado();
        }
      }
    }, 1000);
  }

    componentWillUnmount() {clearInterval(this.intervalo);}

  render() {
    return (
      <section className="row d-none d-md-block">
        <div className="col-12 corpoPainel">
          <a href="https://www.vultr.com/?ref=7208367" target="_blank" rel="noopener noreferrer"><img src="https://www.vultr.com/media/banner_1.png" id="propagandaLinha" width="728" height="90" alt="Vultr" /></a>
        </div>
      </section>
    );
  }
}
