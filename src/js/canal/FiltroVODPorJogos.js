import React, { Component } from 'react';
import CardJogo from '../CardJogo.js'
import LoadingCardJogo from '../loadingComponents/LoadingCardJogo.js';

const $ = window.$;

export default class FiltroVODPorJogos extends Component {

  hash = null;
  cacheJogos = [];
  listaJogosExterna = [];
  componenteAtualizando = false;

  constructor(props) {
    super(props);
    this.state = {
      'listaJogos': [],
      'selecionados': []
    };
  }

  componentDidUpdate() {
    const v = this.props.listaVods;
    if(JSON.stringify(v) === this.hash) return false;
    else this.hash = JSON.stringify(v);

    this.componenteAtualizando = true;

    this.setState({'listaJogos': []});
    this.listaJogosExterna = [];

    const ids = [];
    for(let i = 0; i < v.length; i++) {

      let jogadosNessaLive = [];
      for(let j = 0; j < v[i].linhaDoTempo.length; j++) {

        if(!this.estaNaListaIds(ids, v[i].linhaDoTempo[j].game_id) && v[i].linhaDoTempo[j].game_id !== '0') {
          ids.push({'id': v[i].linhaDoTempo[j].game_id, 'n': 0});
        }

        if(v[i].linhaDoTempo[j].game_id !== '0' && !jogadosNessaLive.includes(v[i].linhaDoTempo[j].game_id)) {
          jogadosNessaLive.push( v[i].linhaDoTempo[j].game_id);
        }

      }

      for(let x = 0; x < jogadosNessaLive.length; x++)
        for(let y = 0; y < ids.length; y++)
          if(ids[y].id === jogadosNessaLive[x])
            ids[y].n++;

    }

    this.componenteAtualizando = false;

    this.iniciarListaJogos(ids);
  }

  iniciarListaJogos(ids) {
    for(let i = 0; i < ids.length; i++) {
      this.getDados(ids[i]);
    }
  }

  estaNaListaIds(lista, id) {
    for(let x = 0; x < lista.length; x++)
      if(lista[x].id === id)
        return true;
    return false;
  }

  addCache(novo) {
    for(let i = 0; i < this.cacheJogos.length; i++)
      if(this.cacheJogos[i].id === novo.id)
        return false;

    this.cacheJogos.push(novo);
    return true;
  }

  getCache(id) {
    for(let i = 0; i < this.cacheJogos.length; i++)
      if(this.cacheJogos[i].id === id)
        return this.cacheJogos[i];

    return null;
  }

  getDados(game) {
    if(this.componenteAtualizando) return false;

    let cache = this.getCache(game.id);
    if(cache !== null) {
      cache.n = game.n;
      const novo = this.listaJogosExterna.slice(0);
      novo.push(cache);
      this.listaJogosExterna = novo;
      this.setState({'listaJogos': novo});
      return true;
    }

    this.ajaxRecast(
      this.props.endpoint+"/jogo/"+game.id,
      {},
      "GET",
      (r) => {

        if(this.componenteAtualizando) return false;
        if(this.estaNaListaIds(this.listaJogosExterna, r.id)) return false;

        this.addCache(r);
        const novo = this.listaJogosExterna.slice(0);
        r.n = game.n;
        novo.push(r);
        this.listaJogosExterna = novo;
        this.setState({'listaJogos': novo});

      },
      () => {}
    );

    return false;
  }

  render() {
    let lista = [
                  <LoadingCardJogo key={0} />,
                  <LoadingCardJogo key={1} />,
                  <LoadingCardJogo key={2} />,
                  <LoadingCardJogo key={3} />,
                  <LoadingCardJogo key={4} />,
                  <LoadingCardJogo key={5} />,
                  <LoadingCardJogo key={6} />
                ];
    if(this.state.listaJogos.length > 0) lista = [];

    const ordenado = this.state.listaJogos.slice(0);
    ordenado.sort((a, b) => {
      if(a.n > b.n) return -1;
      else if(a.n < b.n) return 1;
      return 0;
    });

    for(let x = 0; x < ordenado.length; x++) {
      if(!this.state.selecionados.includes(ordenado[x].id)) {

        lista.push(
          <div key={x} onClick={() => {

              const novo = this.state.selecionados.slice(0);
              novo.push(ordenado[x].id);
              this.setState({'selecionados': novo});
              if(this.props.aplicado !== undefined && this.props.aplicado !== null)
                this.props.aplicado(novo);

            }} style={{
              'display': 'inline-block',
              'cursor': 'pointer',
              'width': '110px',
              'height': '250px',
              'overflow': 'hidden',
            }}>
            <CardJogo nome={ordenado[x].name} nLives={ordenado[x].n} imagem={ordenado[x].box_art_url.replace(/\{width\}/, "140").replace(/\{height\}/, "200")} />
          </div>
        );

      } else {

        lista.push(
          <div key={x} onClick={() => {

              const novo = this.state.selecionados.slice(0);
              novo.splice(novo.indexOf(ordenado[x].id), 1);
              this.setState({'selecionados': novo});
              if(this.props.aplicado !== undefined && this.props.aplicado !== null)
                this.props.aplicado(novo);

            }} style={{
              'display': 'inline-block',
              'cursor': 'pointer',
              'width': '110px',
              'height': '250px',
              'overflow': 'hidden',
            }}>
            <div style={{'display': 'inline-block', 'opacity': 0.5}}>
              <CardJogo nome={ordenado[x].name} nLives={ordenado[x].n} imagem={ordenado[x].box_art_url.replace(/\{width\}/, "140").replace(/\{height\}/, "200")} />
            </div>
            <span className="fa fa-check"
              style={{
                'fontSize': '60px',
                'position': 'relative',
                'top': '-230px',
                'color': '#483d8b'
              }}></span>
        </div>
        );

      }
    }

    return (
      <section className="row">
        <div className="col-12">
          <h4 style={{'textAlign': 'right'}}>Clique em um ou mais jogos para aplicar o filtro</h4><hr />
          <div className="corpoPainel">
            {lista}
          </div>
        </div>
      </section>
    );
  }

  ajaxRecast(url, data, tipo, callbackSucesso, callbackErro) {
    $.ajax({
             url: url,
             data: data,
             type: tipo,
             success: callbackSucesso,
             error: callbackErro
          });
  }

}
