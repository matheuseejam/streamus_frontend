import React, { Component } from 'react';
import CardStreamer from './CardStreamer.js'
import LoadingCardStreamer from './loadingComponents/LoadingCardStreamer.js'
const $ = window.$;

export default class PainelPrincipal extends Component {

  intervaloChecagemResolucao = null;

  constructor(props) {
    super(props);
    this.state = {
      'recastErro': false,
      'listaStreamers': [],
      'dimencoesGrafico': {x: 400, y: 150}
    };

    this.iniciarListaStreamers();
    this.intervaloChecagemResolucao = setInterval(this.checarResolucao.bind(this), 1000);
  }

  checarResolucao() {
    if(window.innerHeight >= window.innerWidth)
      this.setState( {dimencoesGrafico: {x: 225, y: 150}} );
    else
      this.setState( {dimencoesGrafico: {x: 400, y: 150}} );
  }

  componentWillUnmount() {clearInterval(this.intervaloChecagemResolucao);}

  iniciarListaStreamers() {
    this.ajaxRecast(
      this.props.endpoint+"/estatisticas/streamersemascensao",
      {},
      "GET",
      this.finalizarListarStreamers.bind(this),
      () => {this.setState({'recastErro': true});}
    );
  }

  finalizarListarStreamers(lista) {
    if(typeof lista != 'object') return false;

    const filtrada = lista.map((streamer) => {
      let lives = streamer.Viewers.map((x) => {return {Viewers: x};});
      return {nome: streamer.nome, dados: lives, imagem: streamer.imagem};
    });

    this.setState({listaStreamers: filtrada});
  }

  render() {
    var elementos = [<LoadingCardStreamer key={0} />, <LoadingCardStreamer key={1} />];
    if(this.state.listaStreamers.length > 0) elementos = [];

    for(let x = 0; x < this.state.listaStreamers.length && x < 4; x++) {
      elementos.push(<CardStreamer key={x} nome={this.state.listaStreamers[x].nome} imagem={this.state.listaStreamers[x].imagem} dados={this.state.listaStreamers[x].dados} dimencoes={this.state.dimencoesGrafico} endpoint={this.props.endpoint} />);
    }

    if(this.state.recastErro) {
      elementos = (
        <div className="alert alert-danger" role="alert">
          <h4 className="alert-heading"><img src="https://cdn.betterttv.net/frankerfacez_emote/167677/1" alt="FeelsDankMan" /> Deu Ruim!</h4>
          <p>Nosso site tentou buscar os dados no servidor, mas ficou no vácuo. E das duas uma. Ou você tá sem internet, ou o servidor não está funcionando. Sem ofensas, mas espero que seja a primeira opção.</p>
          <hr />
          <p className="mb-0">Já assistiu <a href="http://lmgtfy.com/?q=Rick+And+Morty"><b style={{'color': 'black'}}>Rick And Morty</b></a>? É mó da hora pô.</p>
        </div>
      );
    }

    return (
      <section className="row painelEstatisticas">
        <div className="col-12">
          <h3>Streamers em ascensão</h3><hr />
          <div className="corpoPainel">
            {elementos}
          </div>
        </div>
      </section>
    );
  }

  ajaxRecast(url, data, tipo, callbackSucesso, callbackErro) {
    $.ajax({
             url: url,
             data: data,
             type: tipo,
             success: callbackSucesso,
             error: callbackErro
          });
  }
}
