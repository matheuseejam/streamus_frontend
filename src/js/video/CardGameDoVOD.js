import React, { Component } from 'react';

export default class CardGameDoVOD extends Component {

  render() {

    return (
      <div style={
        {
          'width': '100%',
          'height': '100px',
          'marginTop': '10px'
        }}>

        <img
            src={this.props.box_art_url.replace(/\{width\}/, "70").replace(/\{height\}/, "100")}
            alt="Capa do jogo"
            style={{'float': 'left', 'display': 'block'}}
        />
        <div style={{'marginLeft': '80px'}}>
          <h5 style={{'color': 'white'}}>{this.props.name}</h5>
          <h5 style={{'color': 'white'}}>{"Momento de início: "+this.formatarTempo(this.props.momento)}</h5>
          <h6 style={{'color': 'white', 'textAlign': 'right'}}>
              <span
                style={{'cursor': 'pointer'}}
                onClick={() => {this.props.avancar(this.props.momento);}}
              >
                Clique para avançar
              </span>
          </h6>
        </div>

      </div>
    );

  }

  formatarTempo(bruto) {
    let horas = 0;
    let minutos = 0;
    let segundos = bruto;

    minutos = Math.floor(segundos / 60);
    segundos = segundos % 60;

    horas = Math.floor(minutos / 60);
    minutos = minutos % 60;

    horas = horas.toString();
    if(horas.length === 1) horas = '0'+horas;
    minutos = minutos.toString();
    if(minutos.length === 1) minutos = '0'+minutos;
    segundos = segundos.toString();
    if(segundos.length === 1) segundos = '0'+segundos;

    return horas+':'+minutos+':'+segundos;
  }

}
