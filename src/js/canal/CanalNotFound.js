import React, { Component } from 'react';

export default class CanalNotFound extends Component {

  render() {
    return (
      <div className="row">
        <div className="col-6 offset-3">
          <div className="card text-center" style={{"marginTop": "50px"}}>
            <div className="card-header">
              <img src="https://cdn.betterttv.net/emote/5a9cefea32b4e770a9571d1a/1x" alt="peepoD" /> Canal não encontrado
            </div>
            <div className="card-body">
              <h5 className="card-title">Existem algumas causas possíveis:</h5>
              <p className="card-text">O sistema somente captura canais que transmitem em Português.</p>
              <p className="card-text">Só são capturados canais que obtém mais de 200 viwers simultâneos.</p>
              <p className="card-text">Você pode ter digitado o nome do canal errado.</p>
            </div>
            <div className="card-footer text-muted">
              Pode ser que servidor tenha caído também, mas não conta pra ninguém
            </div>
          </div>
        </div>
      </div>
    );
  }
}
