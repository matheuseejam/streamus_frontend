import React, { Component } from 'react';
import TwitchVideoPlayer from './js/video/TwitchVideoPlayer.js';
import InfosVod from './js/video/InfosVod.js';
import ChatTwitchVOD from './js/video/ChatTwitchVOD.js';
import { ENDPOINT } from './js/GlobalVars';

const $ = window.$;

export default class Video extends Component {

  videoControler = null;
  videoTocando = false;

  constructor(props) {
    super(props);

    this.state = {
      'idVideo': null,
      'dadosVod': null,
      'erroFatal': false
    };

    this.avancarVideo = this.avancarVideo.bind(this);
    this.getMomentoVOD = this.getMomentoVOD.bind(this);
    this.despausado = this.despausado.bind(this);
  }

  componentDidMount() {
    let listaCaminhos = window.location.pathname.split('/');
    let idVideo = listaCaminhos[listaCaminhos.length - 1]; // Último

    this.setState({'idVideo': idVideo}, () => {this.getInfos();});

    document.body.classList.add('videoContainerExterno');
  }

  getMomentoVOD() {
    if(this.videoControler === null || this.videoControler === undefined) return 0;
    return this.videoControler.getCurrentTime();
  }
  despausado() {
    if(this.videoControler === null || this.videoControler === undefined) return false;
    return this.videoTocando;
  }


  getInfos() {
    if(typeof this.state.idVideo !== 'string') return false;

    $.ajax({
             url: ENDPOINT+"/vod/"+this.state.idVideo,
             type: "GET",
             success: (r) => {
              this.setState({'dadosVod': r});
             },
             error: () => {this.setState({'erroFatal': true});}
          });
  }

  avancarVideo(tempo) {
    if(this.videoControler !== null)
      this.videoControler.seek(tempo);
  }

  render() {
    if(this.state.erroFatal) {
      return (<div>
                <a href='/'>
                  <button type="button" className="btn btn-outline-light">
                    <span className="fa fa-arrow-left"></span>  Voltar
                  </button>
                </a>
                <h1
                  style={{
                    'fontSize': '20vmax',
                    'color': 'white',
                    'textAlign': 'center',
                    'marginTop': '20vh'
                  }}>
                  Achei não
                </h1>
              </div>);
    }

    let idStreamer, nomeStreamer = null;
    if(this.state.dadosVod) {
      idStreamer = this.state.dadosVod.user_id;
      nomeStreamer = this.state.dadosVod.user_name;
    }

    return (
      <div className="container-fluid" style={{'maxWidth': '1600px'}}>
        <div className="row">
          <div className="col-12 col-md-9" id="containerVideo">
            <div className="row">

              <div className="col-12" id="containerVideo">
                <div className="embed-responsive embed-responsive-16by9">
                  <TwitchVideoPlayer
                                      width={0}
                                      height={0}
                                      idVideo={this.state.idVideo}
                                      returnVideoControler={(c) => {this.videoControler = c;}}
                                      onPaused={() => {this.videoTocando = false;}}
                                      onPlaying={() => {this.videoTocando = true;}}
                  />
                </div>
              </div>

            </div>

            <div className="row d-none d-md-block">
              <div className="col-12">
                <InfosVod dados={this.state.dadosVod} avancarVideo={this.avancarVideo} getMomentoVOD={this.getMomentoVOD} />
              </div>
            </div>

          </div>
          <div className="col-12 col-md-3">
            <ChatTwitchVOD
                            idVOD={this.state.idVideo}
                            idStreamer={idStreamer}
                            nomeStreamer={nomeStreamer}
                            getMomentoVOD={this.getMomentoVOD}
                            isPlaying={this.despausado}
            />
          </div>
        </div>
      </div>
      );
  }
}
