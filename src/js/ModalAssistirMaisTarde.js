import React, { Component } from 'react';
import { ENDPOINT, LIMITE_ASSISTIR_MAIS_TARDE } from './GlobalVars';
const $ = window.$;

export default class ModalAssistirMaisTarde extends Component {

  hash = null;
  cache = [];
  listaExterna = [];

  constructor(props) {
    super(props);

    this.state = {
      'lista': []
    };

  }

  componentDidUpdate() {
    try {
      if(JSON.stringify(this.props.lista) === this.hash) return false;
      this.hash = JSON.stringify(this.props.lista);
    }
    catch(e) {return false;}

    this.iniciarListagem(this.props.lista);
  }

  iniciarListagem(lista) {
    this.listaExterna = [];
    this.setState({'lista': []}, () => {
      for(let i = 0; i < lista.length; i++)
        this.get(lista[i]);
    });
  }

  addCache(novo) {
    for(let i = 0; i < this.cache.length; i++)
      if(this.cache[i].id === novo.id)
        return false;

    this.cache.push(novo);
    return true;
  }

  getCache(id) {
    for(let i = 0; i < this.cache.length; i++)
      if(this.cache[i].id === id)
        return this.cache[i];

    return null;
  }

  get(id) {

    let cache = this.getCache(id);
    if(cache !== null) {
      const nova = this.listaExterna.slice(0);
      nova.push(cache);
      this.listaExterna = nova;
      this.setState({'lista': nova});

      return true;
    }

    $.ajax({
             url: ENDPOINT+"/vod/"+id+"?removerMomentos",
             type: "GET",
             success: (r) => {
              this.addCache(r);
              const nova = this.listaExterna.slice(0);
              nova.push(r);
              this.listaExterna = nova;
              this.setState({'lista': nova});
             },
             error: () => {}
          });

    return false;
  }

  remover(id) {
    let vetor = this.getListaAssistirMaisTarde();

    let index = vetor.indexOf(id);
    if (index > -1)
      vetor.splice(index, 1);

    const novaLista = [];
    for(var i = 0; i < this.listaExterna.length; i++)
      if(this.listaExterna[i].id !== id)
        novaLista.push(this.listaExterna[i]);

    this.listaExterna = novaLista;
    this.setState({'lista': novaLista});
    this.setCookie('assistirMaisTarde', JSON.stringify(vetor), 365);

    if(this.listaExterna.length === 0)
      $('#modalAssistirMaisTarde').modal('hide');
  }

  getListaAssistirMaisTarde() {
    let cookie = this.getCookie("assistirMaisTarde");
    let vetor = [];

    if(cookie !== "") {
      try {vetor = JSON.parse(cookie);}
      catch(e) {/* NÃO FAÇA NADA */}
    }

    if(!Array.isArray(vetor))
      vetor = [];

    return vetor;
  }

  render() {

    let vods = []
    for(let i = 0; i < this.state.lista.length; i++) {
      vods.push(
                <div className="row" key={i}>
                      <div className="col-5">
                        <a href={"/videos/"+this.state.lista[i].id}>
                          <img
                                src={this.state.lista[i].thumbnail_url.replace(/%\{width\}/, "300").replace(/%\{height\}/, "170")}
                                className="img-thumbnail"
                                style={{'cursor': 'pointer'}}
                                alt="Thumb do VOD"
                          />
                      </a>
                      </div>
                      <div className="col-7">
                        <h5 style={{'float': 'right', 'cursor': 'pointer'}} onClick={() => {this.remover(this.state.lista[i].id);}}><span className="fa fa-trash"></span></h5>
                        <h5>{this.state.lista[i].user_name}</h5>
                        <p>{this.state.lista[i].title}</p>
                      </div>
                  </div>
                );
    }

    if(vods.length === 0)
      vods = [<h1 key={1}>Carregando...</h1>];
    if(vods.length === LIMITE_ASSISTIR_MAIS_TARDE)
      vods.push(<div key={LIMITE_ASSISTIR_MAIS_TARDE} className="alert alert-dark" style={{'textAlign': 'center'}} role="alert">Limite máximo atingido</div>);

    return (
      <div className="modal fade" id="modalAssistirMaisTarde" tabIndex="-1" role="dialog" aria-labelledby="AssistirMaisTarde" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Assistir Mais Tarde</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {vods}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

}
