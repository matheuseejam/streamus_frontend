import React, { Component } from 'react';
import MensagemChatTwitchVOD from './MensagemChatTwitchVOD.js'
import { ENDPOINT, TWITCH_CLIENT_ID } from '../GlobalVars';
const $ = window.$;

export default class ChatTwitchVOD extends Component {

  endpoint = 'https://api.twitch.tv/v5/videos/';
  paginaAtual = null;
  inicializando = true;
  proximaPagina = null;
  chatDeveScrollar = true;
  lastTouchScreenY = Infinity;
  mouseNoChat = false;
  indexMensagemAtual = 0;
  VODPausado = true;
  relogioInterno = 0;
  mensagensNaTela = [];
  montado = false;
  obtendoNovasMensagens = false;
  BTTVEmotes = new Map();
  TwitchBadges = new Map();

  style = {
    'container': {
                    'position': 'fixed',
                    'height': '100vh',
                    'backgroundColor': '#232526',
                    'maxWidth': '500px',
                    'minWidth': '24%'
                  },
    'containerPortrait': {
                    'height': '70vh',
                    'backgroundColor': '#232526',
                    'minWidth': '100%'
                  },
    'containerMensagens': {
                    'overflow': 'auto',
                    'height': '100%'
                  },
    'head': {
      'position': 'absolute',
      'top': '0px',
      'borderBottom': '1px solid white',
      'backgroundColor': 'black',
      'height': '30px',
      'width': '100%'
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      'mensagensNaTela': [],
      'styleContainer': 'container'
     }

    this.sincronizar = this.sincronizar.bind(this);
    this.preencherChat = this.preencherChat.bind(this);
    this.getNovasMensagens = this.getNovasMensagens.bind(this);
    this.request = this.request.bind(this);
    this.posicionarCursor = this.posicionarCursor.bind(this);
    this.sincronizarRelogio = this.sincronizarRelogio.bind(this);
    this.getMomentoVOD = this.getMomentoVOD.bind(this);
    this.setarEstado = this.setarEstado.bind(this);
    this.momentoForaDoRange = this.momentoForaDoRange.bind(this);
    this.movendoChatComDedo = this.movendoChatComDedo.bind(this);
  }

  componentDidMount() {
    this.montado = true;
    //Iniciando chamadas recursivas com setTimeOut
    this.sincronizar();
    this.preencherChat();
    this.getNovasMensagens();
    this.setarEstado();
    //FIM

    this.carregarBTTVEmotes();
    this.carregarBadgesTwitch();

    // Evento nescessário para detectar quando o usuário está tentando
    // Ver mensagens antigas no chat
    window.addEventListener("wheel",
      (event) => {
        if(event.deltaY < 0 && this.mouseNoChat)
          this.chatDeveScrollar = false;
      },
      {
        capture: true,
        passive: true
      }
    );

    this.inicializar();
  }
  componentWillUnmount() {this.montado = false;}

  movendoChatComDedo(evento) {
    const screenY = evento.changedTouches[0].pageY;
    if(screenY > this.lastTouchScreenY) this.chatDeveScrollar = false;
    this.lastTouchScreenY = screenY;
  }

  sincronizar() {
    if(!this.montado) return false;

    const w = window.innerWidth;
    if(w <= 767/*Ponto de quebra entre PC e cel*/) {
      if(this.state.styleContainer !== 'containerPortrait') this.setState({'styleContainer': 'containerPortrait'});
    }
    else {
      if(this.state.styleContainer !== 'container') this.setState({'styleContainer': 'container'});
    }
    this.lastTouchScreenY = Infinity; // Limpando buffer de toque na tela

    if(this.inicializando) {
      setTimeout(this.sincronizar, 1000);
      return;
    }
    if(this.props.getMomentoVOD === undefined || this.props.isPlaying === undefined) {
      setTimeout(this.sincronizar, 1000);
      return;
    }

    if(this.VODPausado && this.props.isPlaying()) {
      this.VODPausado = !this.props.isPlaying();
      this.sincronizarRelogio(this.props.getMomentoVOD());
      setTimeout(this.sincronizar, 1000);
      return;
    }

    this.VODPausado = !this.props.isPlaying();

    const momentoDoVideo = this.props.getMomentoVOD();
    let dif = this.getMomentoVOD() - momentoDoVideo;
    if(dif < 0) dif *= -1;

    if(!this.VODPausado && (this.getMomentoVOD() === 0 || dif > 1)) { // O chat está desincronizado, ou, não está inicializado
      this.sincronizarRelogio(momentoDoVideo);
      if(this.momentoForaDoRange( momentoDoVideo ))
        this.inicializar();
    }

    setTimeout(this.sincronizar, 1000);
  }

  preencherChat() {
    /*
      Caso especial, a página atual acabou, mas ele não paginou ainda
    */
    if(this.paginaAtual !== null &&
      this.indexMensagemAtual >= this.paginaAtual.comments.length &&
      this.proximaPagina !== null) {
        this.paginar();
    }

    if(this.inicializando ||  // Se ainda está inicializando
      this.paginaAtual === null || // Se não existem mensagens para serem lidas
      this.indexMensagemAtual >= this.paginaAtual.comments.length || // Se a página de mensagens já acabou
      this.VODPausado // O vod não está tocando
    ) {
      setTimeout(this.preencherChat, 100);
      return;
    }

    const novas = [];
    let tempo = this.paginaAtual.comments[this.indexMensagemAtual].content_offset_seconds;
    while (tempo - this.getMomentoVOD() < 0.100) {

      novas.push(this.paginaAtual.comments[this.indexMensagemAtual]);

      this.indexMensagemAtual++;
      if(this.indexMensagemAtual >= this.paginaAtual.comments.length) {
        if(this.proximaPagina === null)
          break;
        this.paginar();
      }
      tempo = this.paginaAtual.comments[this.indexMensagemAtual].content_offset_seconds;

    }

    let temp = this.mensagensNaTela.slice(0).concat(novas);
    if(temp.length > 100) temp = temp.slice(-100);

    this.mensagensNaTela = temp;

    setTimeout(this.preencherChat, 100);
  }

  setarEstado() {
    if(!this.montado) return false;

    this.setState({'mensagensNaTela': this.mensagensNaTela}, () => {
      if(this.chatDeveScrollar) {
        var elem = document.getElementById('chatContainerMensagens');
        if(elem !== null) elem.scrollTop = elem.scrollHeight;
      }
    });

    setTimeout(this.setarEstado, 100);
    return true;
  }

  getNovasMensagens() {
    if(!this.montado) return false;

    if(this.inicializando || this.obtendoNovasMensagens) {
      setTimeout(this.getNovasMensagens, 1000);
      return;
    }
    this.obtendoNovasMensagens = true;
    if(this.proximaPagina === null && this.paginaAtual !== null) {
      this.request('cursor='+this.paginaAtual._next,
                  (r) => {
                    this.obtendoNovasMensagens = false;
                    this.proximaPagina = r;
                  },
                  (r) => {
                    this.obtendoNovasMensagens = false;
                    this.getNovasMensagens();
                  }
      );
    }
    else this.obtendoNovasMensagens = false;

    setTimeout(this.getNovasMensagens, 1000);
  }

  paginar() {
    if(this.inicializando) return false;
    this.paginaAtual = this.proximaPagina;
    this.proximaPagina = null;
    this.indexMensagemAtual = 0;
  }

  sincronizarRelogio(offset) {
    let milisecs = Date.now()
    this.relogioInterno = milisecs - Math.floor(offset*1000);
  }

  getMomentoVOD() {
    //Ainda não foi sincronizado
    if(this.relogioInterno === 0) return 0;
    let milisecs = Date.now();

    return ((milisecs - this.relogioInterno)/1000);
  }

  inicializar() {
    if(!this.montado) return false;
    this.inicializando = true;

    this.indexMensagemAtual = 0;
    this.paginaAtual = null;
    this.proximaPagina = null;
    this.mensagensNaTela = [];

    this.request('content_offset_seconds='+this.getMomentoVOD(),
    (r) => {
      this.paginaAtual = r;
      this.inicializando = false;
      this.posicionarCursor();
    },
    (r) => {if(this.montado) this.inicializar();}
    );
  }

  posicionarCursor() {
    if(
      this.paginaAtual === null ||
      this.indexMensagemAtual >= this.paginaAtual.comments.length
    ) {
        return false;
      }

    let tempo = this.paginaAtual.comments[this.indexMensagemAtual].content_offset_seconds;
    while (tempo < this.getMomentoVOD()) {
      this.indexMensagemAtual++;
      if(this.indexMensagemAtual >= this.paginaAtual.comments.length) {
        this.indexMensagemAtual = 0;
        break;
      }
      tempo = this.paginaAtual.comments[this.indexMensagemAtual].content_offset_seconds;
    }

    return true;
  }

  momentoForaDoRange(momento) {
    if(this.paginaAtual === null) return true;
    const inicio = this.paginaAtual.comments[0].content_offset_seconds;
    let fim = this.paginaAtual.comments[this.paginaAtual.comments.length-1].content_offset_seconds;
    if(this.proximaPagina !== null )
      fim = this.proximaPagina.comments[this.proximaPagina.comments.length-1].content_offset_seconds;

    if(momento >= inicio && momento <= fim) return false;
    return true;
  }

  render() {
    let retomarScroll = '';
    if(!this.chatDeveScrollar)
      retomarScroll = <b
                            style={{'color': 'white', 'cursor': 'pointer'}}
                            onClick={() => {this.chatDeveScrollar = true;}}
                        >Retomar rolagem</b>;

    let head = (
      <div style={this.style.head}>
        {retomarScroll}
        <img
              src="../favicon.png"
              alt="StreamUs"
              height="100%"
              style={{'float': 'right'}}
          />
      </div>);

      const mensagens = [];
      for (var i = 0; i < this.state.mensagensNaTela.length; i++)
        mensagens.push(<MensagemChatTwitchVOD
                                              key={this.state.mensagensNaTela[i]._id}
                                              Mensagen={this.state.mensagensNaTela[i]}
                                              Emotes={this.BTTVEmotes}
                                              TwitchBadges={this.TwitchBadges}
                      />);

    return (
      <div className="row no-gutters" style={this.style[this.state.styleContainer]}>
        <div className="col">
          {head}
          <div id="chatContainerMensagens"
                onMouseEnter={() => {this.mouseNoChat = true;}}
                onMouseLeave={() => {this.mouseNoChat = false;}}
                onTouchMove={this.movendoChatComDedo}
                className="scrollbarPersonalizada"
                style={this.style.containerMensagens}>
            {mensagens}
          </div>
        </div>
      </div>
      );
  }

  request(chave, sucesso, falha) {
    return $.ajax({
                     url: this.endpoint+this.props.idVOD+"/comments?"+chave,
                     beforeSend: (request) => {
                                     request.setRequestHeader("client-id", TWITCH_CLIENT_ID);
                                   },
                     type: "GET",
                     success: sucesso,
                     error: falha
                  });
  }

  carregarBadgesTwitch() {
    if(typeof this.props.idStreamer !== 'string') {
      setTimeout(() => {this.carregarBadgesTwitch();}, 1000); // Tente de novo mais tarde
      return false;
    }

    // Consultado API e adicionando ao Map
    $.ajax({
               url: ENDPOINT+'/globalTwitchBadges',
               type: "GET",
               success: (r) => {
                 $.each(r.badge_sets, (chave, valor) => {
                      if(this.TwitchBadges.get(chave) !== undefined) {
                         let tmp = this.TwitchBadges.get(chave);
                         $.each(valor.versions, (chave2, valor2) => {
                           if(tmp[chave2] === undefined) {
                             tmp[chave2] = valor2;
                           }
                         });
                      }
                      else this.TwitchBadges.set(chave, valor.versions);
                  });
               },
               error: () => {/* Empty */}
            });

    $.ajax({
               url: ENDPOINT+'/channelTwitchBadges/'+this.props.idStreamer,
               type: "GET",
               success: (r) => {
                 $.each(r.badge_sets, (chave, valor) => {
                     if(this.TwitchBadges.get(chave) !== undefined) {
                        let tmp = this.TwitchBadges.get(chave);
                        $.each(valor.versions, (chave2, valor2) => {
                          tmp[chave2] = valor2;
                        });
                     }
                     else this.TwitchBadges.set(chave, valor.versions);
                  });
               },
               error: () => {/* Empty */}
            });
  }

  carregarBTTVEmotes() {
    if(typeof this.props.idStreamer !== 'string') {
      setTimeout(() => {this.carregarBTTVEmotes();}, 1000); // Tente de novo mais tarde
      return false;
    }

    // Frankerfacez Global Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/frankerfacez_emotes/global',
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, emote.images['1x']);
                 });
               },
               error: () => {/* Empty */}
            });

    // BTTV Global Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/emotes',
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, 'https://cdn.betterttv.net/emote/'+emote.id+'/1x');
                 });
               },
               error: () => {/* Empty */}
            });

    // Frankerfacez Channel Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/frankerfacez_emotes/channels/'+this.props.idStreamer,
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, emote.images['1x']);
                 });
               },
               error: () => {/* Empty */}
            });

    // BTTV Channel Emotes
    $.ajax({
               url: 'https://api.betterttv.net/2/channels/'+this.props.nomeStreamer,
               type: "GET",
               success: (r) => {
                 r.emotes.forEach((emote) => {
                   this.BTTVEmotes.set(emote.code, 'https://cdn.betterttv.net/emote/'+emote.id+'/1x');
                 });
               },
               error: () => {/* Empty */}
            });

      return true;
  }

}
