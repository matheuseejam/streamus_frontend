import React, { Component } from 'react';
import CardGameDoVOD from './CardGameDoVOD.js'
import { ENDPOINT } from '../GlobalVars';


const $ = window.$;

export default class ListaJogosVOD extends Component {

  listaExterna = [];

  constructor(props) {
    super(props);

    this.state = {
      'lista': []
    }
  }

  componentDidMount() {
    for (var i = 0; i < this.props.lista.length; i++) {
      this.getDados(this.props.lista[i]);
    }
  }

  getDados(jogo) {

    $.ajax({
             url: ENDPOINT+"/jogo/"+jogo.id,
             type: "GET",
             success: (r) => {
              r.momento = jogo.momento;
              if(r.id === '0') r.box_art_url = 'https://static-cdn.jtvnw.net/ttv-static/404_boxart-{width}x{height}.jpg';

              const novo = this.listaExterna.slice(0);
              novo.push(r);

              this.listaExterna = novo;
              this.setState({'lista': novo});

             },
             error: () => {this.getDados(jogo);}
          });

  }

  render() {

    const ordenado = this.state.lista.sort((a, b) => {
      if(a.momento < b.momento) return -1;
      if(a.momento > b.momento) return 1;
      return 0;
    });

    const cards = [];
    for (var i = 0; i < ordenado.length; i++) {
      cards.push(
        <CardGameDoVOD {...ordenado[i]} avancar={this.props.avancar} key={i} />
      );
    }

    return (
      <div className="scrollbarPersonalizada" style={{'height': '200px', 'overflow': 'auto'}}>
        {cards}
      </div>
    );
  }

  formatarTempo(bruto) {
    let horas = 0;
    let minutos = 0;
    let segundos = bruto;

    minutos = Math.floor(segundos / 60);
    segundos = segundos % 60;

    horas = Math.floor(minutos / 60);
    minutos = minutos % 60;

    horas = horas.toString();
    if(horas.length === 1) horas = '0'+horas;
    minutos = minutos.toString();
    if(minutos.length === 1) minutos = '0'+minutos;
    segundos = segundos.toString();
    if(segundos.length === 1) segundos = '0'+segundos;

    return horas+':'+minutos+':'+segundos;
  }

}
