import React, { Component } from 'react';
import Cabecalho from './js/Cabecalho.js';
import PropagandaColuna from './js/PropagandaColuna.js';
import AdBlockDetectado from './js/AdBlockDetectado.js';
import Capa from './js/canal/Capa.js';
import PainelVods from './js/canal/PainelVods.js';
import CanalNotFound from './js/canal/CanalNotFound.js';
import BntLiveAgora from './js/canal/BntLiveAgora.js';

import {ENDPOINT, TWITCH_CLIENT_ID} from './js/GlobalVars.js';
const $  = window.$;

export default class Streamer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      "estado": "carregando",
      'adBlock': false,
      "id": "",
      "login": "",
      "display_name": "",
      "type": "",
      "broadcaster_type": "",
      "description": "",
      "profile_image_url": "",
      "offline_image_url": "",
      "banner": "",
      "view_count": "",
      "momentos": []
    };

    this.adBloqueado = this.adBloqueado.bind(this);
  }

  adBloqueado() {this.setState({'adBlock': true});}

  componentDidMount() {
    $.ajax({
             url: ENDPOINT+"/userByName/" + window.location.pathname.slice(1).toLowerCase(),
             type: "GET",
             success: (r) => {
              if(r !== null && r !== undefined && r.id !== undefined) {
                this.setState(r);
                this.setState({"estado": "ok"});

                this.getProfileBanner(r.login, (pb) => {this.setState({'banner': pb});});

                this.testarImagemDesatualizada(this.state.profile_image_url);
                this.testarImagemDesatualizada(this.state.offline_image_url);
              }
              else { this.setState({"estado": "erro"}); }
             },
             error: () => {this.setState({"estado": "erro"});}
          });
  }


  render() {
    if(this.state.adBlock)
      return <AdBlockDetectado />;

    let tela = "";

    if(this.state.estado === "erro") {
      tela = <CanalNotFound />;
    }
    else if(this.state.estado === "ok") {
      document.title = this.state.display_name + ' - StreamUs';

      tela = (
        <div className="container-fluid">
          <Capa imagemPerfil={this.state.profile_image_url} nome={this.state.display_name} description={this.state.description} banner={this.state.banner} />
        <hr />
        <div className="row">

          <PropagandaColuna bloqueado={this.adBloqueado} />

          <div className="col-md-10">
            <PainelVods idUser={this.state.id} />
          </div>

        </div>
        <BntLiveAgora nomeStreamer={this.state.display_name} idStreamer={this.state.id} />
      </div>
      );

    }

    return (
      <div>
        <Cabecalho /><br />
        {tela}
      </div>
    );
  }

/*

  Sub-Sistema que checa se as imagens do usuários estão desatualizadas
  e avisa ao backend

*/
  testarImagemDesatualizada(url) {
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = (e) => {
        if( xhr.readyState === 4 ) {
          if (xhr.status === 200) {
            if (url !== xhr.responseURL)
              this.alertarUserDesatualizado();
          }
          else this.alertarUserDesatualizado();
        }
      };
      xhr.open("GET", url, true);
      xhr.send();
  }

  alertarUserDesatualizado() {
    var xhr = new XMLHttpRequest();
    xhr.open("PATCH", ENDPOINT+'/userByName/'+this.state.login, true);
    xhr.send();
  }

/*

  A imagem de banner do canal não vem da api do StreamUs.
  Buscando banner na api da Twitch

*/
  getProfileBanner(login, callback) {
    $.ajax({
             url: 'https://api.twitch.tv/kraken/channels/'+login,
             beforeSend: (request) => {
                request.setRequestHeader("client-id", TWITCH_CLIENT_ID);
              },
             type: "GET",
             success: (r) => {callback(r.profile_banner);},
             error: () => {}
          });
  }
}
