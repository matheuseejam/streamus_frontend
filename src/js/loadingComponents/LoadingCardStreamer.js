import React, { Component } from 'react';

export default class LoadingCardStreamer extends Component {
  render() {
    return (
      <div className="loadingComponent loadingCardStreamer">
          <div className="animated-background" style={{'backgroundSize': '800px 104px', 'height': '120px'}}>
              <div className="background-masker" style={{'top': '0px', 'left': '100px', 'right': '0px', 'height': '100px'}}></div>
              <div className="background-masker" style={{'top': '100px', 'left': '0px', 'width': 'calc(100%)', 'height': '10px'}}></div>
              <div className="background-masker" style={{'top': '110px', 'left': '100px', 'width': 'calc(100% - 100px)', 'height': '10px'}}></div>
          </div>
      </div>
    );
  }
}
