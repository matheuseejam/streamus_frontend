import React, { Component } from 'react';

export default class LoadingStreamer extends Component {
  render() {
    return (
      <div className="loadingComponent loadingStreamer">
          <div className="animated-background" style={{'backgroundSize': '800px 104px', 'height': '95px'}}>
              <div className="background-masker" style={{'top': '0px', 'left': '85px', 'right': '0px', 'height': '20px'}}></div>
              <div className="background-masker" style={{'top': '20px', 'left': '85px', 'width': '7px', 'height': '15px'}}></div>
              <div className="background-masker" style={{'top': '35px', 'left': '85px', 'right': '0px', 'height': '20px'}}></div>
              <div className="background-masker" style={{'top': '55px', 'left': '85px', 'width': '7px', 'height': '15px'}}></div>
              <div className="background-masker" style={{'top': '65px', 'left': '85px', 'right': '0px', 'height': '30px'}}></div>
          </div>
      </div>
    );
  }
}
