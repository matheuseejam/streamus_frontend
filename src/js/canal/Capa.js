import React, { Component } from 'react';

export default class Capa extends Component {

  estilos = {
    container: {"height": "200px", "overflow": "hidden"},
    banner: {"position": "relative", "top": "-25%"},
    description: {"position": "absolute", "bottom": "0", "left": "50px", "color": "white", "textShadow": "2px 2px 5px black", "width": "calc(100% - 75px)"},
    imagemPerfil: {'height': '150px', 'margin': 'auto', 'display': 'block'},
    nome: {'textAlign': 'center', 'width': '150px', 'wordBreak': 'break-word', 'margin': 'auto'},
    portrait: {
      imagemPerfil: {'height': '100px', 'marginRight': 'auto', 'display': 'block'},
      nome: {'textAlign': 'center', 'width': '100px', 'wordBreak': 'break-word', 'fontSize': '14pt', 'textShadow': "0px 0px 2vw white"}
    }
  };

  render() {
    return (
      <div className="row" style={this.estilos.container}>

        <div className="col-md-2 d-none d-md-block">
          <div style={{'marginTop': 'auto'}}>
            <img src={this.props.imagemPerfil} className="img-thumbnail" style={this.estilos.imagemPerfil} alt="Imagem de perfil" />
            <h5 style={this.estilos.nome}>{this.props.nome}</h5>
          </div>
        </div>
        <div className="col-md-10 d-none d-md-block">
          <img src={this.props.banner} width="100%" style={this.estilos.banner} alt="Imagem de capa" />
          <h5 style={this.estilos.description}>{this.props.description}</h5>
        </div>

        <div className="col-12 d-md-none" style={{
            'backgroundImage': 'url('+this.props.banner+')',
            'backgroundRepeat': 'no-repeat',
            'backgroundPosition': 'center',
            'backgroundSize': 'auto 100%',
            'display': 'flex'
          }}>
          <div style={{'marginTop': 'auto'}}>
            <img src={this.props.imagemPerfil} className="img-thumbnail" style={this.estilos.portrait.imagemPerfil} alt="Imagem de perfil" />
            <h5 style={this.estilos.portrait.nome}>{this.props.nome}</h5>
          </div>
        </div>
      </div>
    );
  }
}
