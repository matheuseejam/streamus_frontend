import React, { Component } from 'react';
import CardJogo from './CardJogo.js'
import LoadingCardJogo from './loadingComponents/LoadingCardJogo.js';

const $ = window.$;

export default class PrincipaisJogos extends Component {

  constructor(props) {
    super(props);
    this.state = {
      'recastErro': false,
      'listaJogos': []
    };

    this.iniciarListaJogos();
  }

  iniciarListaJogos() {
    this.ajaxRecast(
      this.props.endpoint+"/estatisticas/maisjogados",
      {},
      "GET",
      this.finalizarListarJogos.bind(this),
      () => {this.setState({'recastErro': true});}
    );
  }

  finalizarListarJogos(lista) {
    const filtrada = lista.filter((j) => {return j.idJogo !== 0;});
    this.setState({listaJogos: filtrada});
  }

  render() {
    let lista = [
                  <LoadingCardJogo key={0} />,
                  <LoadingCardJogo key={1} />,
                  <LoadingCardJogo key={2} />,
                  <LoadingCardJogo key={3} />,
                  <LoadingCardJogo key={4} />,
                  <LoadingCardJogo key={5} />,
                  <LoadingCardJogo key={6} />
                ];
    if(this.state.listaJogos.length > 0) lista = [];

    let limite = this.state.listaJogos.length;
    if(limite > 20) limite = 20;

    for(let x = 0; x < limite; x++) {
      lista.push(<CardJogo key={x} nome={this.state.listaJogos[x].nome} nLives={this.state.listaJogos[x].nLives} imagem={this.state.listaJogos[x].imagem.replace(/\{width\}/, "140").replace(/\{height\}/, "200")} />)
    }

    if(this.state.recastErro) {
      lista = (
        <div className="alert alert-danger" role="alert">
          <h4 className="alert-heading"><img src="https://cdn.betterttv.net/frankerfacez_emote/167677/1" alt="FeelsDankMan" /> Deu Ruim!</h4>
          <p>Nosso site tentou buscar os dados no servidor, mas ficou no vácuo. E das duas uma. Ou você tá sem internet, ou o servidor não está funcionando. Sem ofensas, mas espero que seja a primeira opção.</p>
          <hr />
          <p className="mb-0">Já assistiu <a href="http://lmgtfy.com/?q=Rick+And+Morty"><b style={{'color': 'black'}}>Rick And Morty</b></a>? É mó da hora pô.</p>
        </div>
      );
    }

    return (
      <section className="row painelEstatisticas">
        <div className="col-12">
          <h4>Mais jogados da semana</h4><hr />
          <div className="corpoPainel">
            {lista}
          </div>
        </div>
      </section>
    );
  }

  ajaxRecast(url, data, tipo, callbackSucesso, callbackErro) {
    $.ajax({
             url: url,
             data: data,
             type: tipo,
             success: callbackSucesso,
             error: callbackErro
          });
  }

}
