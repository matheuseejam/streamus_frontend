import React, { Component } from 'react';
import ListaJogosVOD from './ListaJogosVOD.js'
import { AreaChart, Area, Tooltip, ResponsiveContainer, ReferenceLine } from 'recharts';

export default class InfosVod extends Component {

  intervalo = null;

  constructor(props) {
    super(props);

    this.state = {'momento': 0};
  }

  componentDidMount() {
    this.intervalo = setInterval(() => {
      if(this.props.getMomentoVOD !== undefined && this.props.getMomentoVOD !== null)
        this.setState({'momento': this.props.getMomentoVOD()});
    }, 1000);
  }
  componentWillUnmount() {clearInterval(this.intervalo);}

  render() {
    if(this.props.dados === undefined || this.props.dados === null) return '';

    const dados = this.processarDados(this.props.dados);
    const indexMomento = this.indexFromListaMomentos(this.state.momento, dados.linhaDoTempo);

    return (
      <div>
        <div className="row">
          <div className="col-12 col-md-4">
            <div className="row">
              <div className="col-12">
                <h6 style={{'color': 'white'}}>Assistindo VOD de {dados.data}</h6>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-12">
                <a href={'/'+this.props.dados.user_name}>
                  <button type="button" className="btn btn-outline-light">
                    <span className="fa fa-arrow-left"></span>  {this.props.dados.user_name}
                  </button>
                </a>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-8 mt-2">
            <ListaJogosVOD lista={dados.listaJogos} avancar={this.props.avancarVideo} />
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <ResponsiveContainer width="100%" height={200}>
              <AreaChart
                        data={dados.linhaDoTempo}
                        margin={{top: 10, right: 0, left: 0, bottom: 0}}
                        onClick={(x) => {if(x.activePayload[0]) this.props.avancarVideo(x.activePayload[0].payload.momento);}}
              >
                <Tooltip />
                <Area
                      type='monotone'
                      dataKey='viewer_count'
                      stroke='#034F84'
                      fill='#95DEE3'
                />
              <ReferenceLine x={indexMomento} stroke="#034F84" />
              </AreaChart>
            </ResponsiveContainer>
          </div>
        </div>

      </div>
      );
  }

  indexFromListaMomentos(momento, lista) {
    for (var i = 1; i < lista.length; i++) {
      if(momento > lista[i-1].momento && momento < lista[i].momento) {
        return i-1;
      }
    }
    return 0;
  }

  processarDados(bruto) {
    let d = new Date(bruto.started_at*1000);
    const processado = {
      'data': d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(),
      'linhaDoTempo': null,
      'listaJogos': []
    };

    bruto.linhaDoTempo.sort((a, b) => {
      if(a.momento > b.momento) return 1;
      if(a.momento < b.momento) return -1;
      return 0;
    });

    let offset = bruto.linhaDoTempo[0].momento - bruto.started_at;
    if(offset < 0) offset = bruto.linhaDoTempo[0].momento;
    else offset = bruto.started_at;

    processado.linhaDoTempo = bruto.linhaDoTempo.map((momento) => {
      return {
        "game_id": momento.game_id,
    		"momento": momento.momento-offset,
    		"viewer_count": momento.viewer_count
      }
    });

    let jogoAtual = null;
    for(let i = 0; i < processado.linhaDoTempo.length; i++) {
      if(processado.linhaDoTempo[i].game_id !== jogoAtual || jogoAtual === null) {
        processado.listaJogos.push(
          {
            'id': processado.linhaDoTempo[i].game_id,
            'momento': processado.linhaDoTempo[i].momento
          }
        );
        jogoAtual = processado.linhaDoTempo[i].game_id;
      }
    }

    return processado;
  }


}
