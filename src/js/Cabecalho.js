import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ModalAssistirMaisTarde from './ModalAssistirMaisTarde.js';
import { ENDPOINT } from './GlobalVars';

const $ = window.$;

export default class Cabecalho extends Component {

  intervalo = null;
  mostrandoSugestoes = false;
  ultimaBuscaStreamer = 0;

  constructor(props) {
    super(props);

    this.state = {
      'mostrarBntAssistirMaisTarde': 'none',
      'sugestoesBusca': [],
      'lista': []
    };

    this.checarCookie = this.checarCookie.bind(this);
    this.testeContinuo = this.testeContinuo.bind(this);
    this.efetuarBusca = this.efetuarBusca.bind(this);
    this.mostrarPainelSugestoes = this.mostrarPainelSugestoes.bind(this);
    this.esconderPainelSugestoes = this.esconderPainelSugestoes.bind(this);
  }

  componentDidMount() {
    /*
      Por causa de um conflito entre o react e o bootstrap, existe a possibilidade
      da classe que aplica fundo na rota /videos/* permanecar nas outras rotas do app
      Essa linha remove essa classe cso ela esteja presente
    */
    $("body").removeClass("videoContainerExterno");

    this.intervalo = setInterval(this.testeContinuo, 1000);
  }
  componentWillUnmount() {clearInterval(this.intervalo);}

  mostrarPainelSugestoes() {
    if(!this.mostrandoSugestoes) {
      $("#painelSugestBusca").toggle();
      this.mostrandoSugestoes = true;
    }
  }
  esconderPainelSugestoes() {
    if(this.mostrandoSugestoes) {
      $("#painelSugestBusca").toggle();
      this.mostrandoSugestoes = false;
    }
  }

  testeContinuo() {
    if(this.checarCookie()) {

      if(this.state.mostrarBntAssistirMaisTarde === 'none') {
        this.setState({
          'mostrarBntAssistirMaisTarde': 'flex',
          'lista': JSON.parse(this.getCookie('assistirMaisTarde'))
        });
      }
      else this.setState({'lista': JSON.parse(this.getCookie('assistirMaisTarde'))});

    }
    else {
      if(this.state.mostrarBntAssistirMaisTarde === 'flex') {
        this.setState({
          'mostrarBntAssistirMaisTarde': 'none',
          'lista': []
        });
      }
    }
  }

  checarCookie() {
    let bruto = this.getCookie('assistirMaisTarde');
    if(bruto === "") return false;

    let lista = null
    try {lista = JSON.parse(bruto);}
    catch(e) {return false;}

    if(!Array.isArray(lista) || lista.length === 0) return false;

    for(let i = 0; i < lista.length; i++)
      if(typeof lista[i] !== 'string')
        return false;

    return true;
  }

  efetuarBusca() {
    if(Date.now() - this.ultimaBuscaStreamer < 200) return false;

    const nome = $('#inputBusca').val();
    if(nome.length === 0) this.esconderPainelSugestoes();
    if(nome.length < 4) return false;

    this.ultimaBuscaStreamer = Date.now();
    $.ajax({
             url: ENDPOINT+"/searchUser/"+nome,
             type: "GET",
             success: (r) => {
              this.setState({'sugestoesBusca': r});
              this.mostrarPainelSugestoes();
             },
             error: () => {}
          });

    return true;
  }

  render() {

    const sugestoes = this.state.sugestoesBusca.map((nome) => {
      return (<a key={nome} className="dropdown-item" href={'/'+nome}>{nome}</a>);
    });

    return (
        <nav className="navbar navbar-expand-lg navbarStreamUS">

          <Link to="/" style={{"color": "inherit", "textDecoration": "none"}}>
            <img src="logo.png" width="40px" height="30px" className="d-block d-md-none" alt="StreamUS" />
            <h4 id="tituloNavBar" className="d-none d-md-block">StreamUS<small className="text-muted" id="versionNavbar"> Alpha</small></h4>
          </Link>

          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBarCabecalho" aria-controls="navBarCabecalho" aria-expanded="false" aria-label="Toggle navigation">
            <span className="fa fa-bars" style={{'color': 'white', 'fontSize': '20pt'}}></span>
          </button>

          <div className="collapse navbar-collapse" id="navBarCabecalho">
            <ul className="navbar-nav ml-auto">
              <li className={"nav-item mt-1 ml-auto d-"+this.state.mostrarBntAssistirMaisTarde}>
                <button type="button" className="btn btn-outline-success ml-auto" data-toggle="modal" data-target="#modalAssistirMaisTarde">Assistir mais tarde</button>
              </li>
              <li className="nav-item mt-1 mb-1 ml-auto">
                  <div className="input-group">
                    <input
                            type="text"
                            className="form-control"
                            onChange={this.efetuarBusca}
                            placeholder="Buscar Streamer"
                            aria-label="Buscar Streamer"
                            id="inputBusca"
                            autoComplete="off"
                    />
                    <div className="input-group-append">
                      <span className="input-group-text"><span className="fa fa-search" aria-hidden="true"></span></span>
                    </div>
                    <div id='painelSugestBusca' className="dropdown-menu" aria-labelledby="Dropdown">
                      {sugestoes}
                    </div>
                  </div>
              </li>
            </ul>
          </div>
          <ModalAssistirMaisTarde lista={this.state.lista} />
        </nav>
    );
  }


  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
}
